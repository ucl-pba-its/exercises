# Øvelse 15 - Powershell script

### Information

I denne øvelse skal i sammen i jeres gruppe skrive nmapscan bash scriptet fra tidligere, denne gang i powershell.  
Formen er at i sammen omskriver og finder metoder til at skrive et powershell script med samme funktionalitet som bash scriptet fra øvelse 11.

![script](nmapscans_example.png)

### Instruktioner

1. Installer nmap i windows, kan findes her [https://nmap.org/](https://nmap.org/)
2. Omskriv nmapscan scriptet fra sidste uge til pseudo kode som alle i gruppen forstår.
3. Omskriv bash scriptet til et powershell script.

## Links
