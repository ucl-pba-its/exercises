# Øvelse 18 - Seriel forbindelse til vsrx virtuel router

### Information

I denne øvelse skal du bruge _putty_ og _windows named pipes_ til at forbinde til vsrx konsollen.  
Du kan godt bruge konsollen direkte i vmware men min oplevelse er at den er langsom, det er også lettere at kopiere tekst til/fra putty.

Hvis du er på linux eller mac så vil det her ikke virke da _named pipes_ er en windows ting. Jeg kom så langt som at finde noget i osx der hedder _mkfifo_ men jeg har ikke en mac og kan ikke teste det.  
Hvis du er på mac, kan du måske bruge tiden på at finde en løsning til mac ?

![vrsx_intro_til_itsik.png](vrsx_intro_til_itsik.png)  
*Netværksdiagram*

### Instruktioner

1. Hent og installer putty hvs du ikke allerede har det på din computer [https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
2. tilføj en seriel port til vsrx i vm settings
3. rediger seriel porten til _use named pipe_ og brug syntax `\\.\pipe\<pipenavn>`
4. åben putty og konfigurer en seriel forbindelse (serial line) med pipe navnet, baudrate 9600, 8 databits, 1 stopbit, parity + flowcontrol none
5. gem putty konfigurationen
6. boot vsrx
7. forbind med putty og konfirmer at du får output fra din vsrx vm
8. default user er `root` og intet password (det konfigureres i næste øvelse)

### Links

Per fra IT-Teknolog lavet en video om hvordan det gøres [https://youtu.be/ft5UBqMAlRw](https://youtu.be/ft5UBqMAlRw)
