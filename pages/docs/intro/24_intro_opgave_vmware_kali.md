# Øvelse 24 - Kali Linux på vmware

## Information

Denne øvelse handler om at installere vmware workstation og derefter installere Kali Linux i en virtuel maskine.

VMware workstation eren hypervisor som gør det muligt at køre virtuelle maskiner på din computer.
Kali Linux er en Linux distribution målerettet IT sikkerhed og har et stort katalog af værktøjer som bruges af IT sikkerheds professionelle.  

## Instruktioner

1. Download VMWare Workstation Pro Trial version [https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html](https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html)
2. Installer VMware workstation pro
3. Download Kali Linux til VMware [https://www.kali.org/get-kali/#kali-virtual-machines](https://www.kali.org/get-kali/#kali-virtual-machines)
4. Installer Kali Linux ved hjælp af denne guide [https://www.kali.org/docs/virtualization/import-premade-vmware/](https://www.kali.org/docs/virtualization/import-premade-vmware/)
5. Start Kali Linux VM og log ind, Default credentials er kali/kali
6. Åbn en terminal og ping `8.8.8.8` for at sikre at du har forbindelse til internettet

## Links

- Kali Linux dokumentation [https://www.kali.org/docs/](https://www.kali.org/docs/)
- VMware workstation pro 16 user guide [https://docs.vmware.com/en/VMware-Workstation-Pro/16.0/workstation-pro-16-user-guide.pdf](https://docs.vmware.com/en/VMware-Workstation-Pro/16.0/workstation-pro-16-user-guide.pdf)