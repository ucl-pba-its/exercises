# Øvelse 50 - Overblik over fagets øvelser og læringsmål

### Information

I har igennem semestret arbejdet med fagets læringsmål igennem forskellige øvelser.  
I denne øvelse skal i danne jer et overblik over hvad i har arbejdet med og hvor i har brug for at repetere op til eksamen.

### Instruktioner

1. Lav et dokument med en overskrift for hver øvelse beskrevet i fagets materiale via itslearning
2. Gennemse fagets læringsmål på itslearning
3. For hver øvelse noter hvad du mangler i forhold til læringsmål.

## Links
