# Øvelse 51 - Eksamens forberedelse

### Information

Til eksamen bør du forberede en præsentation for hvert spørgsmål du kan trække.  
Formålet med denne øvelse er at få et overblik over eksamens spørgsmålene og hvad du kan læse op på inden eksamen.

### Instruktioner

1. Læs eksamens øvelsen på itslearning
2. Sammenhold eksamens spørgsmålene med dit dokument fra **øvelse 52 - Repetition af faget** og noter hvad du vil inkludere i din besvarelse af spørgsmålene.
3. Forbered præsentationer og evt. demoer, husk at overveje hvordan du kan inddrage projektarbejdet i din præsentation.

## Links
