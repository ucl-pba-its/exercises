# Øvelse 7 - Præsentationsplan

## Information

At kunne kommunikere skriftligt og mundtligt er en stor del af at arbejde med it sikkerhed. Du skal kunne kommunikere din viden og ekspertise og sætte den i kontekst for andre, der ikke har samme viden og ekspertise.  
For eksempel har ledelse et andet fokus, ofte økonomi, så for at få dem til at forstå f.eks. investeringer i sikkerheds tiltag, skal du kunne formidle din viden på en måde der gør din leder i stand til at træffe de rette beslutninger.
Nysgerrighed og research er en anden vigtig kompetence. Du er nok allerede vant til at skulle sætte dig ind i nye teknologier eller værktøjer.  
Denne øvelse er ikke anderledes, du vil få trænet din evne til at researche et emne og samtidig overveje hvordan du kan formidle din viden til andre.

Endelig afsluttes de fleste fag med en mundtlig eksamen, også i denne kontekst er ovenstående vigtigt at træne.

Alle studerende skal i løbet af semestret præsentere et emne der er indeholdt i faget "introduktion til it sikkerhed", det er ok hvis i går to sammen om en præsentation, kravet er at i alle skal præsentere mindst en gang i løbet af semestret.

Der er afsat 30 minutter til hver præsentation inkl. spørgsmål.

Emnerne er:

- Programmering i Python med sockets
- Programmering i Python med databaser
- Bash Scripting
- Powershell scripting
- Netværk - osi model, wireshark samt vigtigste netværksprotokoller og deres sikkerhedsniveau
- Kali linux (f.eks. nmap og metasploit)

## Instruktioner

1. Gå sammen 2 og 2 og snak om hvad der er vigtigt i en præsentation ud fra nedenstående spørgmål.
   - Hvem er dit publikum ? Hvad ved de i forvejen og hvad optager dem ?
   - Hvordan laver du overblik over det du vil præsentere og hvordan kan du skabe flow i din præsentation ?
   - Hvor meget information skal du have på hver af dine slides ? Hvad er essensen af det du præsenterer på det enkelte slide ?
   - Vil du involvere dit publikum og få dem til at udføre ting i løbet af din præsentation ? Vil du demonstrere noget live ? Hvordan ? Hvordan styrer du tid ?
   - Hvad med referencer og kilder til det du præsenterer ? Hvordan kan du dele det med dit publikum så de selv kan finde mere information ?
   - Hvordan agere du selv under præsentationen ? Hvordan skaber du kontakt til og engagement med dit publikum ?
2. Skriv essensen af jeres overvejelser ned
3. Gå til planen på itslearning og skriv dit navn på et af emnerne i "rækkefølge oplæg" linket.
4. Øvelsen afsluttes på klassen med en kort runde af hvad i har valgt at præsentere og hvilke overvejelser i har gjort omkring arbejdet med præsentationen.

## Links
