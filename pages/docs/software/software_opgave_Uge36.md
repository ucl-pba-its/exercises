# Opgaver Uge 36

Opgaverne delt op i 2 dele.
Den første del er vidensdeling, og den anden del er konkrette
færdigheder ift. software. I skal altid starte med videndelings øvelserne.

## Videndelings opgaver
Vidensdelings opgaverne giver teamet mulighed for at afstemme og dele
viden internt i teamet, og med andre teams.

Ofte anvendes CL strukturen til vidensdelings øvelser. Dette sikre at alle får taletid, og bliver hørt af øvrige team medlemmer.

Den CL struktur der skal anvendes til hver enkelt øvelse kan finde på
IT's learning under _rescourcer -> Cooperative learning strukturer_

Det er en god ide at aftale hvem der tager tid, inden hver enkelt øvelse
påbegyndes.

Alle vidensdeling øvelser tager udgangspunkt i den læsning som har været
dagens forberedelse.

### Nedpriortering af sikkerheds opgaver i softwareudvikling
_Tid: 10 minutter_  
Teamet skal bruge CL strukturen _Ordet rundt_ til at hver komme med sine
refleksioner og overvejelser på hvorfor Sikkerheds opgaver har en tendens
til at blive nedpriorteret i en softwareudviklings proces.
### Sikkerhed per design.
_Tid: 10 minutter tænketid - 10 minutter diskussion_  
Teamet skal bruge CL strukturen _Møde på midten_ til at dele sine refleksioner
om hvorfor Sikkerhed per design kan være en bedre fremgangsmåde end at betragte
sikkerhed som en feature.
### Tre(to) til te.
_Tid: 10 minutter_  
Teamet skal bruge CL strukturen _Tre til te_ til at lære hvordan de 
andre grupper opfatter sikkerhed pr. design.  
_Selvom denne struktur hedder tre til te, passer to til te nok bedre med team størrelserne_

## Software
Software opgaverne tager udgangspunkt i konkrette færdigheder ift. til software udvikling.
Idag fokuserer opgaverne på opsætning af udviklingsmiljø samt grundlæggende kompetencer med
C#.

**Forventningen til disse opgaver er altid at disse er løst inden den kommende uges lektioner.**

### Installation af .Net SDK.
SDK står for software development kit. og er et sæt udviklingsværktøjer som skal bruges i forbindelse med udvikling af software i et givet sprog (C#).  
I denne øvelse skal du installer [.Net6 SDK](https://dotnet.microsoft.com/en-us/download/dotnet/6.0). Du kan blot vælge den seneste release.   
_Vær opmærksom på at du vælger SDK og ikke blot runtime_

**.Net SDK indeholder følgende:**  
  
- .Net CLI. Et command line værktøj og compiler [.Net CLI](https://docs.microsoft.com/en-us/dotnet/core/tools/)  
- .Net runtime og standard bibloteker.  

Cli værktøjet kan benyttes til at lave nye projekter eller et compile af et nyt projekt.  
Runtime er et krav for at koden kan eksekveres på operativ systemet.

### Installation af IDE (Eller text editor)  
I denne øvelse skal du installer en IDE(Eller text editor) efter eget valg, tre muligheder som kan fremhæves er:  
  
- [Visual studio Community edition](https://visualstudio.microsoft.com/vs/community/). Fordelen ved denne IDE er at den er skræddersyet til udvikling i C#, og kommer med en mange gode indbygget værktøjer. Den kan dog godt føles en anelse tung at arbejde med.  
- [VS code](https://code.visualstudio.com/).  Er egentlig mere en text editor end IDE. Kan bruges til rigtig mange forskellige ting. Men har god understøttelse af C#, hvis man installer de rigtige extensions.  
- [Rider](https://www.jetbrains.com/rider/). Jetbrains som  har lavet rider er muligvis de bedste på marked til at lave IDE'er. Rider koster som udgangspunkt penge, men i kan hente den gratis hvis i opretter en studie konto hos Jetbrains.  
  
Hvis det er første gang du skal udvikle i C# er visual studio eller VS code formodentlig det bedste udgangspunkt. VS Code kan også bruges i andre fag på studiet. Undertegnet bruger VS code.

### Hello World Konsol applikation
I denne øvelse skal du lave en grundlæggende konsol application med [Hello world tutorial](https://dotnet.microsoft.com/en-us/learn/dotnet/hello-world-tutorial/create).

Reflekter over hvilket steder i koden der bruges et api/standard biblotek? Kan du identificerer et læringsmål i faget som passer med dette?

### Installer applikationen Postman.
I denne øvelse skal du installer applikationen [Postman](https://www.postman.com/downloads/). I skal benytte jeres skole mail ved konto oprettelse.  

Begrebet API betyder enten Application programming interface. Og henviser enten til hvordan man interagerer med et andet objekt/biblotek i kilde kode, eller hvordan man interegere med et andet software system over netværk via F.eks HTTP protokolen. Postman er et grafisk værktøj til at teste interaktionen med HTTP API'er.

### Hello world med HTTP API.
I denne øvelse skal du lave en simple HTTP server med C# & .Net.

**Du skal noget af tutorialen [C# HttpListener](https://zetcode.com/csharp/httplistener/) og gøre følgende:**  
1. Implementerer koden der vist i sektionen med over skriften _"C# HttpListener status"_  
2. Lav et HTTP GET request til API'et fra postman, og verificerer at svaret er som forventet.  
3. Tilføj en response header med navnet "SimpleServer" og værdien "Hello World!".  
 _Obs! hvad skal der altid foran navnet på en "custom" header?_  
4. Lav igen et HTTP GET request til API'et fra postman. Verificer at svaret indeholder headeren.   
5. Brug koden fra sektionen _"C# HttpListener send text"_ til at tilføje en tekststreng i request svaret.
6. Brug postman til at lave et GET request, og verificer at svaret indeholder tekststrengen.

_Typisk når man vil lave HTTP API'er med .Net/C# bruger man frameworket ASP.Net core, men vi starter ud med lidt mindre værktøjer_

### Oprydning
Som udgangspunkt er sårbarheder i software fejl lavet under implementeringen. Det er derfor vigtig at den kode man har
skrevet er så let læst som muligt. Så bliver det nemlig nemmere at opdage fejl.

Du skal gennemgå den kode du har skrevet, og se om du kan gøre den pænere og mere læsbar (F.eks. ved at undlade forkortelser eller ved brug af metoder)

### Bonus opgave
Prøv at eksperimenterer med httplistener. Kan du F.eks ekstrakter et URL parameter.

## Information

## Instruktioner

## Links


