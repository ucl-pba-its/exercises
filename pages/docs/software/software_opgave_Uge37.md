# Opgaver Uge 37

Opgaverne delt op i 2 dele.
Den første del er vidensdeling, og den anden del er konkrette
færdigheder ift. software udvikling. I skal altid starte med videndelings øvelserne.

## Videndelings opgaver
Vidensdelings opgaverne giver teamet mulighed for at afstemme og dele
viden internt i teamet.  
  
Det er en god ide at aftale hvem der tager tid, inden hver enkelt opgave
påbegyndes.

Alle videns deling opgaver tager udgangspunkt i den læsning som har været
dagens forberedelse.

### Hvorfor lave en modellering af forretnings domæne, og hvorfor ikke? 
_Tid: 10 minutter_  
I denne opgave skal teamet diskuterer hvorfor man bør lave en modellering af foretningsdomæne.
Teamet skal dele sig op i 2 hold. et der taler for, og et der
taler imod modellering af forretnings domænet.  
_Noter alle argumenter for og imod_
### Overfladisk modellering (Shallow modelling)
_Tid: 5 minutter  
I denne opgave skal gruppen diskuterer hvad der menes med begrebet _"Overfladisk modellering"._  
### Dybdegående modellering (Deep modelling)
_Tid: 5 minutter_  
I denne opgave skal teamet diskutere hvad der menes med begrebet _"dybdegående modellering"._

## Software

Øvelserne er idag delt i to.
Den første del er en kort første introduktion til domæne modellering,
og i den anden del skal i forsat arbejde med basale C# programmerings kompetencer.


### Domæne modellering af en webshop (Team opgave)
_tid: 30_  
I denne øvelse skal teamet lave en domæne modellering af en webshop.
  
Her skal der tages udgangspunkt i den notations teknik i bliver introduceret for i dokumentet
_"Intro til objekt og domænemodel"_.

Husk at domæne modellering **Ikke** er system design. Men noget vi laver for at
forstå de koncepter som eksisterer i det forretnings domæne som vores system skal arbejde med.  
  
_Forventningen er ikke at i laver en perfekt domæne model eller mestre det, blot at i bliver introduceret til konceptet og kan se samhængen med kapitelet i har læst til idag._

### Opret en klasse.
Formålet med de følgende øvelser er at introducerer jer til brugen af klasser i C#.
Senere i undervisningen kommer vi til at bruge klasser til at sikre at der kun er valid data i vores system  
og som et værn mod diverse injection angreb (F.eks. Stored XSS eller arbitary code injection). 

**Gør følgende:**  
1. lav en fil i httplistener projektets folder som hedder Person.cs_  
2. Skriv den rigtig syntaks for at definer en [klasse](https://docs.microsoft.com/en-us/dotnet/csharp/fundamentals/types/classes) den nye fil.  
_// symbolerne er blot kommentar, disse kan du undlade_.  
3. Ændre navnet på klassen så den passer til filnavnet.  
_En klasse har pr. konvention altid samme navn som det fil den er defineret i, dog Uden .cs extension_  
4. Tilføj en [Constructor](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/constructors) til din klasse.  
_Undlad argumenterne som er vist eksemplet_  
5. Tilføj argumenter til constructoren, som passer til den data klassen bør have.
_Hvilken data kunne være relevant når klassen definerer en person_   
6. Lav private [fields](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/fields) i klassen af samme type og navn som argumenterne i constructoren.  
7. I constructeren, skal jeres private fields tildels værdi fra det dertil svarende constructor argument.  
8. Kontroller at klassen er implementeret korrekt ved at instantierer et objekt af typen(klassen) Person i httplistener __ applikationen. [Using constructor](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/using-constructors)  
_Der er ikke tilføjet ny funktionalitet til jeres applikation, men hvis applikationen fejler ved opstart, er der fejl i jeres implementering_  
9. Prøv at instantierer klassen med constructor argumenter som ikke passer med typen, F.eks. String hvor det  skal være typen skulle havde været en int, hvad sker når i prøver at starte applikationen?
10. Reflekter over hvad fordelen ved type safety er, når i F.eks. kun kan bruge objekter af en bestemt type som parameter til constructeren.  


## Opret en metode  
I denne opgave skal der oprettes en metode i Person klassen.  

1. I klassen Person skal der nu oprettes en [metode](https://dotnetcademy.net/Learn/2039/Pages/7) for hver private field.  
Hver metode skal returner værdien af det tilsvarende private field.  
_Navngiv metoden som F.eks GetName el. GetAge_
2. Lav nu metode kald på den klasse der tidligere blev instantieret.
3. verificerer at implementeringen er korrekt.
4. Opret endnu en metode i klassen. Denne gang skal metoden tage imod en værdi, og ændre værdien i et private field.

## Brug property istedet for private field.
I denne opgave private fields skiftes ud med properties.

1. Erstat alle private fields i Person klassen med [property](https://docs.microsoft.com/en-us/dotnet/csharp/properties)  
_Alle properties skal havde getters og setters som vist i kode eksempel 2 i dokumentationen_
2. Sørg for at tildele de nye properties en værdi med argumenterne i constructeren.
3. Undersøg om du kan tilgå en property på det Person objekt du initialiseret i en tidligere øvelse.
4. Undersøg om du kan tildele en property værdi gennem det Person objekt du initialiseret i en tidligere øvelse.
5. skriv _private_ foran _set_ i dine properties.
6. Undersøg om du stadig kan tildele en property værdi via det Person objekt du initialiseret i en tidligere øvelse.  
_Det skal ikke være tilfældet_


## oprydning
Metoderne der blev implementeret tidligere er ikke længere nødvendige og bør fjernes fra koden.  
Herefter bør du sikre dig at eksperimenterne med at tilgå metoder og properties i HttpListener klassen bliver fjernet.

Sikre dig at applikationen virker ved at starte den, og lav et request med postman.

## konvertere objekt til json
Brug denne [guide](https://www.tutorialsteacher.com/articles/convert-object-to-json-in-csharp) til konvertere jeres Person objekt (Den i instantieret tidligere ) til en JSON string,
og returner denne tekst når der laves et get request.  
_Husk at kontroller med postman. i kan også prøve powershell [Webrequest](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/invoke-webrequest?view=powershell-7.2) istedet._

### Reference mellem objekter.
I denne opgave skal der oprettes endnu et klasse kaldet _Pet_ (Tildel den et par properties).  
_Person_ klassen skal nu have en property af typen _Pet_. (Husk at tilføje Pet som constructor argument).

Start applikationen og lav et request med postman. Hvordan ser dataen nu ud?

### Bonus opgave
Prøv at lave en property som er et [array](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/arrays/) af typen PET.

## Information

## Instruktioner

## Links


