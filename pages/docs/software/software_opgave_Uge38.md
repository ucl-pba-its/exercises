# Opgaver Uge 38

Opgaverne delt op i 2 dele.
Den første del er vidensdeling, og den anden del er konkrette
færdigheder ift. software udvikling. I skal altid starte med videndelings øvelserne.

## Videndelings opgaver
Vidensdelings opgaverne giver teamet mulighed for at afstemme og dele
viden internt i teamet.  

Alle vidensdeling opgaver tager udgangspunkt i den læsning som har været
dagens forberedelse.

### Hvad bør en model indeholde?  
  
_Tid: 5 minutter_  
I denne øvelse skal teamet diskuterer hvad en domæne model bør indeholde, og ikke indeholde. 

### ordvalg i domæne modellen.  
  
_Tid: 5 minutter_  
I denne øvelse skal teamet diskuterer hvorfor ensartet ordvalg(sprog) er vigtig i
en domæne model, og hvorfor det er vigtigt at det afspejler sig i koden.  

### Den rigtige model.
_Tid: 5 minutter_  
I denne øvelse skal teamet diskuterer hvorfor der ikke nødvendigvis findes en
enkel "korrekt"  model.

### Data validitet.
_Tid: 5 minutter_  
I denne øvelse skal teamet diskuterer hvordan en domæne model kan give indsigt
hvilken data der er valid data.

## Software

Øvelserne er idag delt i to.
Den første del er team øvelse i domæne modellering,
og i den anden del skal der arbejdes med Klasser og invariance.


### Domæne modellering ud fra data (Team øvelse)
_tid: 15 minutter_  
På nedstående billede er et udsnit af data som benyttes af et gartneri til at holde
styr på hvilken drivhus diverse produktionsbakker opbevares i.  
![ALT](./Images/GartneriData.jpg)  
  
Ud fra dataen skal der nu udarbejdes følgende:  
  
1. En objekt model jvf. dataen.
2. En domæne model skal udledes fra objekt modellen.
3. Fastslå afgrænsninger for hvad dataen i modellen må indeholde. (F.eks. hvilken karakter en tekst streng må indeholde) 
   

### Domæne modellering af SOME applikation (Team øvelse)
_tid: 30 minutter_ 
  
I denne opgave udarbejdes følgende:
  
1. en objekt model af SOME applikationens domæne.
2. Udlede en domæne model fra objekt modellen.
3. Fastslå afgrænsninger for hvad dataen i modellen må indeholde.  
  
_Det er vigtig at huske at det er domænet og ikke systemet i modeller_

### Invariance i en klasse
Invariance er en begrænsning man pålægger specifikke objekter/variabler i en klasse. Et eksempel kunne være at et navn kun
må indeholde alfabetiske tegn. Ift. sikkerhed vil en sådan afgrænsning sikre mod blandt diverse injection
angreb. I de følgende opgaver skal der implementeres invariance i klasser

#### Invariance i Person klassen
  
I Person klassen som blev implementeret i forrige lektion, skal der nu implementeres invariance.
Begrænsningerne er som følger:  
  
- Alder må højst være 100 år, og ikke under 0 år.
- Alle navne må kun indeholde alfabetiske karakterer.
- Et navn må ikke være længere end 15 karakter.  
  
Hvis en begrænsning ikke bliver overholdt skal der "smides" en exception. Dette gøre med
med med kommandoen:  
"throw new exception(< Tekst som forklar hvorfor exceptionen blev smidt her >)"
  
Til opgaven bliver der brug for [if statement](https://www.w3schools.com/cs/cs_conditions.php) og til
kontrollen af karakterer i tekst strengen bliver der brug for [Regex](https://www.tutorialspoint.com/csharp/csharp_regular_expressions.htm).
Til kontrollen af længden på navnet skal du bruge google (Get string length).  
  
Husk at prøve begrænsningerne af ved at lave et nyt objekt med både gyldig og ugyldig værdier.

### Klasser med Python.
I denne opgave skal Person klassen implementeres i Python.
Hvis du bruger VS code kan du med fordel installer Python extension i VS code.

Følgende skal gøres:
  
- Opret en file ved navn _main.py_
- I samme folder skal du oprette endnu en file ved navn _Person.py_
- I filen _Person.py_ skal  du definerer klasse. Kig på [Eksemplet](https://www.w3schools.com/python/python_classes.asp)  
_Constructor i Python defineres med navnet init, tag et kig på den i eksemplet_
- i filen _main.py_ skal klassen importeres. skriv _from Person import Person_ i toppen af filen.
- Lav et Person Objekt i _main.py_.


### Bonus opgave
Implementer Invariance i Python klassen Person.

## Information

## Instruktioner

## Links


