# Opgaver Uge 39

Opgaverne delt op i 2 dele.
Den første del er vidensdeling, og den anden del er konkrette
færdigheder ift. software udvikling. I skal altid starte med videndelings øvelserne.  
  
## Videndelings opgaver (i teams)  
Vidensdeling øvelserne er idag anderledes end tidligere, nu skal teamet i fællesskab analyser
noget kode. Koden finder i ved at klone [dette](https://github.com/dotnet-architecture/eShopOnWeb) repository.
Repo'et(kodebasen) indeholder flere  projekteter, og udgør tilsammen et systemt som understøtter en webshop.
Kodenbasen er lille, men ligener ellers hvad man kan forvente at finde i et produktion miljø.
den del i skal kigge på, kan findes i folderen _src\PublicApi_  
  
### Klasser  
_Tid: 10 minutter_  
Teamet skal diskuterer hvilket formål klasser har har. Hvorfor bliver de anvendt?  
  
### Authentication  
_Tid: 10 minutter_  
Teamet skal identificerer alt den kode som har noget med authentication at gøre.  
_Her kan klasse navne hjælpe_  
  
### Sikker kode konstruktion  
_Tid: 10 minutter_  
Teamet skal kigge på koden til klassen _AuthenticationEndpoint.UserInfo_.  
Vurder om en udvikler utilsigtet kan komme til at ændre på et objekter som er    
initialiseret fra denne klasse.    
_F.eks IsAuthenticated_    
  
### AuthenticateEndpoint  
_Tid: 5 minutter_  
Teamet skal kigge på klassen _AuthenticationEndpoint.AuthenticationRequest_.
Vurder om en udvikler utilsigtet kan komme til at ændre på et objekter som er  
initialiseret fra denne klasse.     
  
### Secure by design  
_Tid: 5 minutter_  
Udfra det teamet indtil videre har lært om klasser og invariance skal teamet nu  
vurderer om koden lever op til _Secure by design principper_, og om applikationen  
potentielt set indeholder _single point of failure_.  
  
### Forbedrings forslag  
_tid: 5 minutter_  
Teamet skal udfra deres observationer, lave forbedrings forslag til 2 klasser.  
  
## Software  
I dagens software øvelser skal der arbejedes med  _validation_. I den første opgave skal der arbejdes med at indlæse data fra
en fil ind i software. Efterfølgende skal der arbejdes med valideringen af filen udfra 4 af de fem kriterier i blev præsenteret for   
  
Hver gang vi krydser en tilids grænse (trust boundary), så skal den
data vi modtager valideres. Dette kan være når vores applikationer modtager data over netværk,
eller når den indlæser fra F.eks. en file.

### Indlæsning af en fil
I denne opgave skal der indlæses en fil. Du skal i din projekt folder oprette en tekst file
med en skrevet tekst. For at indlæse filen i dit program kan du følge (Denne)[https://learn.microsoft.com/en-us/dotnet/standard/io/how-to-read-text-from-a-file] tutorial.

### Size - Kontrol af størrelsen inden indlæsning.  
I forberedelsen til idag, bliver der på side 103 beskrevet 5 punkter som du skal være særlig
opmærksom på ift. til validering. I denne Opgave arbejder vi med _size_.

Inden du indlæser filen skal evaluer om filen er større end 100 bytes.
[Denne](https://www.thecodebuzz.com/how-to-get-file-size-in-csharp/) tutorial kan hjælpe dig på vej.

### Lexical content - med regex
I opgaverne fra forrige uge arbejdet i med regex for at håndhæve invarians i _Person_ klassen.
Denne gang skal i bruge regex igen, dog til at sikre at filen i læser kun indeholder alfabetiske
bogstaver og numeriske tal.  
Første eksempel i [Denne](https://www.techiedelight.com/check-string-consists-alphanumeric-characters-csharp/) guide kan hjælpe på vej  
  
### Syntaks - Det korrekt format  
I denne opgave skal du ændre din tekst file således at den indeholder data i valid json format, F.eks.  
{  
    "Name":"Martin",  
    "Age": 37  
}
   
Herefter skal du lave en klasse med public properties som svare til de værdier json filen indeholder, og konventerer teksten fra filen til et sådan objekt.  
Se eksemplet [her](https://learn.microsoft.com/en-us/dotnet/standard/serialization/system-text-json/how-to?pivots=dotnet-6-0) i afsnittet _How to read JSON as .NET objects_  
  _Prøv at lav en format der ikke er Json, hvad sker der når man forsøger at konventerer det?_

### Semantik
Den semantiske validering af dataen kan blandt udføres via håndhævelse af invariance.  
I klassen som du defineret i forrige øvelse, skal du nu lave begrænsninger på hvilken værdi
properties må have.  
_Når du bruger JSonSerializer kan vi ikke lave kohåndhøve invariance i constructeren, istedet skal det gøres med property setteren_  
[Property i C#](https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/using-properties)  
  

### Bonus opgave
Lav øvelserne, dog denne gang med Python.

## Information

## Instruktioner

## Links


