# Opgaver Uge 40  
  
Opgaverne er delt op i 2 dele.
Den første del er vidensdeling, og den anden del er konkrette
færdigheder ift. software udvikling. I skal altid starte med videndelings øvelserne.  
  
## Videndelings opgaver (i teams)  
  
### Læringsmål i faget Software sikkerhed.  
_Tid: 15 minutter_  
I denne øvelse skal teamet sammen gennemgå alle læringsmålene for faget software sikkerhed,
og noterer/kortlægge hvilken nogle læringsmål vi har arbejdet med indtil videre.  
_Et mindmap kan muligvis hjælpe jer her_

### Overlap mellem læringsmål i fagene Software sikkerhed og Web applikation sikkerhed.
_Tid: 15 minutter_  
I denne øvelse skal teamet sammen gennemgå alle læringsmålene for faget software sikkerhed,
og Web applikation sikkerhed, og noterer de læringsmål der overlapper, samt hvilket emner der  
overlapper.  
 
  
## Software  
  
  Sofware opgaverne er idag delt op i to dele. I den første del skal i arbejde med to værktøjer som kan udføre statisk
  kode analyse. Og i den anden del skal i arbejede med at implementerer klasser med domæne primitiver.
  
### Taint analyse  
En _Taint analyse_ af kildekode er en automatisk analyse hvor flowet i kildekode undersøges og potentielle svagheder detekteres. Da Taint analysen typisk udføres på  
kildekode falder det under kategorien _SAST_(Static application security test), og SAST høre til i kategorien statisk kode analyse.  

I de følgende øvelser skal i udføre en taint analyse af kildekoden til en C# applikation, og herefter skal der analyseres kildekode til en (velkendt) Python applikation.  
    
#### Taint analyse af Flask application  
*Alle kommandoer er testet med Windows powershell og Python 3.10*  
*Hvis du ikke benytter Windows, kan du med fordele lave opgaverne samme en windows bruger, da variationer ift. OS kan forekomme.*  
  
I denne øvelse skal der udføres en taint analyse på den oprindelige kildekode(den uændret udgave) til jeres Python flask applikation.  
  
*gør følgende*:  
1. Brug Git til at klone projektet fra [Repoet]([applikation](https://github.com/Silverbaq/ITSec-ImageSharing))  
2. I powershell, skift folder så i er i folderen med projektet i netop har klonet.  
3. Opret et virtuelt python miljø med kommandoen _py -m venv env_  
4. Start det virtuelle Python miljø med kommandoen _.\env\Scripts\activate_  
5. Brug kommandoen _pip3 install -r requirements.txt_ til at installer alle dependencies.  
6. Brug kommandoen _pip3 install bandit_ til at installer SAST værktøjet bandit.  
7. Udfør kode analysen med kommandoen _bandit ImageSharing.py_  
8. Læs analyse rapporten igennem.  
  
Reflekter over hvilken type potentielle svagheder værktøjet finder, men også hvilken nogen  
den ikke detektere (F.eks. ift. Security by design).
  
#### Taint analyse af en C#(.Net) applikation.  
I denne øvelse skal du udføre en taint analyse på kildekoden til et C# projekt. Til opgaven skal værktøjet visual code grepper anvendes. 
   
*Visual code grepper er kun kompatibelt med windows. Så benytter du dig af Linux eller ligende skal du udføre denne øvelse sammen med et team medlem som benytter Windows*
  
*Gør følgende*:  
1. Download _Visual code grepper_ [Herfra](https://sourceforge.net/projects/visualcodegrepp/).  
2. Installer _Visual code grepper_ ved at eksekverer den downloaded  .msi file.  
3. Klone [dette](https://github.com/dotnet-architecture/eShopOnWeb) repo med git.  
4. Start _Visual code grepper_ (klik på windows start ikonet i process linjen og søg på navnet).  
5. Vælg _Settings->C#_   
6. Vælg _file -> New Target Directory_  
7. I pop-up vinduet skal du navigere hen til folderen som indeholder det klonet projekt, og her skal du vælge under folderen PublicApi (_src->PublicAPi_)  
8. Vælg _Scan->Full scan_  
9. Læs analyse rapporten igennem.  
  
Reflekter over hvilken type potentielle svagheder værktøjet finder, men også hvilken nogen
den ikke detektere. F.eks. konstateret vi i forrige lektion at AuthenticateEndpoint.AuthenticateReqeust.cs klassen
ikke overholder _Secure by design_ principperne.  
  
### Domæne primitiver i C# og Python  
I disse opgaver skal der implementeres klasser som benytter sig at domæne primitiver fremfor system primitiver (Såsom int,char eller double).
Som beskrevet i dagens forberende læsning kan sikkerheden i den software man bygger forberedes gennem brugen af domæne primitiver.
Tidligere er der blevet vist at alder kan repræsenteres med en int variable i Person klassen. Hvis denne klasse istedet er bygget med domæne primitiver,
ville alder stadig havde sin egen variable, men istedet for int ville typen være defineret som klassen Age. Her er det vigtig at nævne at klassen Age
skal håndhæve invariance for at kvalificerer sig som en domæne primitiv.  
  
En god tommelfinger regel for at vurderer hvornår man bør lave klasser som er opbygget af domæne primitiver er at kigge på sin domæne model.
Hvis klassen man bygger også er repræsenteret i domæne modellen, er den ofte en god kandidat til at skulle bygges med domæne primitiver.
I dette tilfælde vil hver attribute som er vist i domæne modellen skulle implementeres som en klasse, og disse klaser er hvad der kaldes domæne primitiver.   
  
F.eks hvis den konceptuelle model(domæne model) for en bil således ud:  
![ALT](./Images/BilKonceptuel.jpg)  
  
Så kunne definationen af modellen bil se således ud i koden:  
![ALT](./Images/CarWithDomainPrimitives.png)  
_Farve,Årgang & registreringsNummer er alle domæne primitiver_
  
Domæne primitivet for Registreringsnummer ser således ud:  
![ALT](./Images/DomainPrimitiveExample.png)  
_Her er det vigtigt at huske at domæne primitiver altid er immutable, Altså de kan ikke ændres. Ydermere er det vigtigt at de håndhæver invariance._   
  

#### Implementering af Person med domæne primitiver i C#    
I denne opgave skal der implementeres en Person klasse, opbygget med domæne
primitiver. I klassen skal følgende være repræsenteret som domæne primitiver:  
  
- Fornavn. _Må kun indeholde alfabetiske bogstaver og maks havde en længde på 20 karakterer_  
- Efternavn. _Må kun indeholde alfabetiske bogstaver længde på 20 karakterer_  
- Alder. _Skal som minnimum være 18 år og må maksimalt være 18 år_  
  

### CPR nummer i Person klassen  
I denne opgave skal Person klassen som blev implementeret i forrige øvelse have en ny domæne primtiv.  
CPR nummer skal tilføjes som domæne primitiv, kravene til den håndhævet invariance er:  
  
- Formattet skal være i formattet xxxxxx-xx.  
  _Regular expression er: ^[0-9]{6}\-[1-9]{4}$_
  
Herudover skal CPR implementeres jvf. Read-once mønsteret (Side 121 ).  
Således sikre vi at CPR ikke uhensigtmæssigt logges i applikationen.  
  
_Jeg har bevidst undladt at håndhæve kravet om lige/ulige tal ift. køn. Men i en virkelig case ville vi også skulle gøre dette via invariance, så vi kigger på det senere i  semesteret_  
  
### Reflekterer over secure by design( & Privacy by design), Domæne primitiver, read-once mønster og Taint analyse.  
Secure by design vægter tilgange/praksisser såsom invariance, Domæne primitiver, read-once mønster o.l. som oftes ikke
detekteres af DAST tools. Hvorfor bør inspektion for sårbarheder i kode(Eksisterende såvel som potentielt kommende)
ikke alene udføres af et DAST tool (Ved udførelse af en taint analyse), men også med en visuel inspektion af koden?(Og derfor havde viden om hvordan koden bør se ud).  
_I reflektionen bør i også overveje hvordan sårbarhederne opstår i software, hvem er det som introducerer dem?_
  
### Person i Python  
Implementer Person klassen med domæne primitiver i Python.  
  
### Reflekterer over Konsekvensen af manglen på type safety i Python.  
_Bemærk at ordet type ofte bliver brugt som synonym for klasse, altså en klasse kaldes også en type_  
  
C# er meget strengt ift. anvendelse af typer(klasser). F.eks kan man ikke bygge(compile) en eksekvererbar applikation udfra 
C# kildekode såfremt deres ikke er overenstemmelse mellem typer. Python derimod er mere dynamisk. Reflekterer over
om det åbner op for flere potentielle sårbarheder grundet fejl/bugs.  
  
### User i Python.    
I denne opgave skal i implementerer en User klasse i Python. I skal anvende domæne primitiver til alle variabler.
  
_Hvilken invariance skal der håndhæves i hver domæne primitive, og bør Read-once mønstret benyttes?_
  
## Information  

## Instruktioner  

## Links  
