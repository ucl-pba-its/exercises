# Opgaver Uge 41  
  
Opgaverne er delt op i 2 dele.
Den første del er vidensdeling, og den anden del er konkrette
færdigheder ift. software udvikling. I skal altid starte med videndelings øvelserne.  
  
## Videndelings opgaver (i teams)  


### Elevation of privilege
_tid: 60 minutter_  
  
I har læst dokumentet "Introduction to threat modelling" som forberedelse til idag.  
I dokumentet bliver der på side 6(pdf side) beskrevet et spil der hedder "Elevation of privilege". Det spil skal i spille.  
Udgangspunktet for trusselsmodelleringen er jeres flask applikation. Gør følgende:  
1. lav jeres diagram (Jvf. side 3-5 i dokumentet).  
2. Læs reglerne (side 6)  
3. Spil.  
  
_Jeg har været nød til at improviser en smule for at "lave" kortene. I må gerne passe på dem selvom de er lavet af papir._  
_Noter jeres modellering, måske det kan bruges til eksamens rapporter(Diagrammet og trusslerne, ikke jeres point score)_  
  
## Software  

### Strongly typed vs dynamically typed  
I undervisningen har vi arbejdet en del med C#, det primær mål med C# har været at give en forståelse for hvilken hjælp et _strongly static typed_ sprog  
kan give ift. at undgå fejl og potentielle sårbarheder i koden. F.eks. inkapsulering af variabler (private fields i C#), domæne primitiver og read-once pattern  
gode eksempel på dette. Og der er en del populærer strongly static typed sprog, F.eks. C#,Java,typescript for at nævne nogle få, men der er også en del populære  
dynamiske sprog, Eksempelvis Javascript og Python. Disse sprog bliver og kaldet _duck type_, hvilket betyder at hvis to objekter har den samme metode implementeret vil 
det som udgangspunkt ikke skabe en fejl. Dette skal afprøves i denne opgave. opgaven skal udføres i samme projekt hvor i implementeret Python Person klasse, i forrige lektion.  
Gør gør følgende:  
  
1. Implementer en klasse som har nøjagtig samme metoder som en af domæne primitiverne (F.eks. Name).  
2. lav en objekt fra jvf. klassen fra forrige trin.  
3. Lav et Person objekt. I constructor argumentet skal i bruge argumentet fra forrige trin.  
4. Observer hvad der sker. Er vi sikre på at den nye klasse håndhæver samme regler som den tidligere(invariance)?  
   
Man kan dog lave en form for type safety, se min løsning fra uge 40.
  
### Grundlæggende brug af logging i Python  
Hidtil er _print_ blevet benyttede til lave applikations output når vi har arbejdet med programmering.  
Dette ændre vi nu, og benytter istedet en logger. I denne opgave skal følgende udføres:  
  
1. Opret en ny Python applikation(folder samt en file, F.eks Main.py).  
2. Implementer en logger samt brugen af logger som vist i det første eksempel i denne [tutorial](https://www.delftstack.com/howto/python/python-logging-to-file-and-console/)  
3. Eksekver applikationen. Og obseverer hvad der sker. Hvilken log niveau bliver ikke vist?  
4. Ændre log opsætning således at den manglende log niveau bliver vist.  
5. Filen _debug.log_ bør være blevet automatisk oprettede i den folder hvor din Python applikation er placeret. Hvad indeholder den?  
6. Istedet for file _debug.log_ skal filen hedde _application.log_. Lav denne ændring i logger konfigurationen  
7. Når der logges bliver loggen også udskrevet til systemets standard output, hvilken del af log konfigurationen gør dette?  
  
**logger skal anvendes til alt output fra nu af, aldrig mere bruge print**  
  
### Try and except  
Konceptetet _Try and catch_ er tidligere blevet introduceret i C#. Python har et lignende koncept som hedder _Try and except_.   
I de følgende øvelser skal der arbejdes med _Try and except_ i Python.  
  
#### Grundlæggende fejl håndtering med _try and except_  
I denne opgave skal der arbejdes med den grundlæggende anvendelse af _Try and except_.  
  
1. Opret en ny Python applikation(Husk at lave en logger opsætning).  
2. Lav et objekt med den Python klasse i implementeret i forrige lektion (med domæne primitiver).  
3. Opret en _try and except_ jvf. det første kode eksempel [her](https://www.w3schools.com/python/python_try_except.asp)  
4. Alle objekter skal oprettes under _Try_ ( Her skal print(x) fra tutorial eksemplet udskiftes med oprettelsen af alle objekterne)   
5. Under _except_ skal der laves en fejl loggning med teksten _Failed to create person object_  
6. Eksekver applikationen og sikre dig at overstående log ikke bliver bliver lavet. (Altså at objektet bliver oprettede korrekt)  
7. opret nu en af domæne primitiverne med en ikke valid værdi. F.eks. et navn som indeholder tal.  
8. Eksekver applikationen og verificerer at der bliver logget en fejl med teksten _"Failed to create person object"_  
9. Efter _Try and except_ skal der laves en info logning som med beskeden _"I made it here!"_.  
10. Eksekver applikationen og verificerer at der bliver logget en info med teksten _"I made it here!"_.  
11. Bibehold alt kode men fjern _Try and except_ (Og kun _Try and except_).  
12. Eksekver applikationen igen, hvad sker der?  
13. Refleketer over hvad der sker når der bliver kastet en Exception uden _Try and except_ kontra med.  
  
#### Håndtering af Exception message  
I den forrige øvelse blev den tekst som skulle logges skrevet eksplicit ind som en string. I denne opgave skal  
fejl meddelsen fra den exception som bliver kastet benyttes som log besked istedet.  
1. Sikre dig at du laver _Raise Exception_ med en besked. Hvis det ikke er tilfælde kan du se et eksempel [her](https://www.w3schools.com/python/gloss_python_raise.asp)  
2. Except skal have en tilføjelse således den ser ud som følgende: _except Exception as exception_. Et eksempel kan ses [her](https://www.adamsmith.haus/python/answers/how-to-catch-and-print-exception-messages-in-python)  
3. under except skal i logge fejlen med følgende linje: _logger.error(exception)_  
4. Eksekverer programmet, og observer hvilken besked der bliver logget.  
5. Reflekterer over hvilken data/information man ikke bør indkluderer i en exception besked. _F.eks. PII_  
  
#### logging af stacktrace  
Nogle udviklerer logger hele stacktracen ved for at bedre kunne finde frem til fejl, hvilket kan være ok i et udviklingsmiljø, men mindre hensigtsmæssigt i et produktions miljø.  
I denne opgave skal i logge stacktracen, fremfor exception beskeden.  
1. udskift _logger.error_ fra forrige øvelse med _logger.exception_.  
2. Eksekver applikationen og observer hvad der sker?  
3. Reflekterer over om det har konsekvenser hvis en angriber kan få fat i stacktracen? (Og F.eks. se at det er en sql exception som blev kastet)  
  
#### Stack trace i webapplikationer  
Ofte benytter udviklerer stacktracen til at fejlfinde i software. Ved web applikationer tillader udviklerer ofte at stacktracen  bliver sendt til  
browseren i tilfælde af fejl. Dette er ok når man udvikler på softwaren. Men der har været en del tilfælde hvor det ikke er blevet slået fra i produktions  
miløjet. I denne ofte skal der eksperimenteres med dette.  
  
1. Klon det oprindelige SOME flask [applikation](https://github.com/Silverbaq/ITSec-ImageSharing)  
2. I toppen filen af _ImageSharing.py_ (linje 10 i min editor). skal DEBUG variablen ændres til værdien True. (DEBUG=True)  
3. Start applikationen  
4. Klik på log in  
5. I username skal du skrive _bad'_(husk apostrophe)  
6. klik på _Login_ knappen.  
7. Observerer hvad der sker. Reflektere over om en angriber kan bruge dette som indgangsvinkel til et angreb? Og hvilket angreb?  
  
### Error handlers  
  
De fleste web applikation frameworks har en mekanisme til at håndtere fejl. og gør følgende:  
1. Sæt igen Debug til false.  
2. I template folderen, lave en kopi af 404.html og navngiv den 500.html  
3. I filen 500.html, erstat sætningen _Ups! This does not exist_ med sætning _Ups! an error happen on our side_  
4. I ImageSharing.py filen, tilføje følgende kode(I bunden):  
![Error handler code](Images/ErrorHandler.jpg)  
5. eksekver applikationen igen.  
6. I username skal du skrive _bad'_  
7. Observerer hvad der sker.  
  
_Bemærk at errorhandleren modtager et exception object. Dette bør dog ikke bruges til at sende fejl meddelse videre til brugeren_  
  
### Custom exception i Python  
I forberedelses læsningen til idag, anbefaler forfatteren at man deler exceptions op mellem tekniske fejl, og fejl ift. forretnings regler.  
Dette kan gøres ved F.eks at implementere sin egen exception type. I denne opgave skal der implementerer to forskellige exception typer.  
En teknisk Exception og en forretnings Exception.  
  
1. Lav en ny Python Applikation (Folder med en Main.py file)  
2. Implementerer begge exception typer.   
   Exceptions implementeres ligesom en klasse, dog skal klasse navnet efterfølges af en parantes med Exception type som parameter, F.eks _class BussinessException(Exception):_    
   I [denne](https://www.pythontutorial.net/python-oop/python-custom-exception/) tutorial kan i se nogle eksempler.
3. Lav en klasse med to metoder. En der kaster en Teknisk exception og en som kaster en foretnings exception. 
4. Lav et objekt med klassen fra trin 3.  
5. lav en try except. Denne gang skal der være flere except. Som vist på billede herunder  
![multiple expect](Images/TryExpect.jpg)  
6. Kald på skift en af de to metoder, og se hvilken expect der griber Exceptionen.  
7. I klassen fra trin tre, skal der implementeres en trejde metode. Denne metode skal kaste en runtime exception    
 _raise RuntimeError("message")_  
8. Kald metoden fra forrige trin i try, og se hvilken exception der griber den.  
  
### Logging i flask  
Der er flere måder hvorpå man kan håndter logging i sin flask applikation. I denne opgave bruger vi den logger opsætning blev lavet i den første opgave.  
Gør følgende i den oprindelige SOME flask applikation (Som blev klonet i en tidligere øvelse):  
  
1.  i toppen af ImageSharing.py skal skal i importer logging og sys.  
2.  Genbrug konfiguration fra den første øvelse, der ser således ud:  
![Logging configuration](Images/LoggingConfiguration.jpg)
3. Lav et info log når der bliver routet til _login_  
  
  
Dette er blot grundlæggende logging i flask, google  kan hjælpe jer med andre opsætninger, F.eks. hvis man vil logge til et andet API.  
  
### Reflektion over logging.  
Afledt af et incident på CTF igår, skal i reflekter over om logging til en lokal file er hensigstmæssigt (Som vi har gjordt i opgaverne idag),   
F.eks. Hvad sker der hvis en hel server er blevet "slettet"?.  




## Information  

## Instruktioner  

## Links  

 @app.errorhandler(500)   
    def Unexpected_error(exception):  
    return render_template('500.html'),   
    500
  