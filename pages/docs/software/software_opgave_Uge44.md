# Opgaver Uge 44  
   
## Grundlæggende unit test  
I denne opgave skal du implementerer din første unit testen.  
  
1. implementerer testen [**Basic examlple**](https://docs.python.org/3/library/unittest.html) Unlad at blot copy/paste koden, men skriv  den manuelt ind.  
2. Eksekver testen. Se kommandoen under [**Command-line Interface**](https://docs.python.org/3/library/unittest.html) i dokumentationen.  
3. Eksekver testen ved brug af test discovery. (søg i den linket dokumentationen på test discovery )  

I den implementeret test skal du nu reflekterer overfølgende:    
1. Er navne på de implementeret 3 tests gode? (fortæller hvad der testes & hvad det forventet resultat er?)  
2. Hvorfor modtager klassen _TestStringMethods_ et parameter? (Prøv søg på Inheritance in Python)  
3. Hvor kommer assert metoderne fra?  
4. hvad tester testen _test_split_ for?  
5. Kan vi benytte unit tests til at sikre at klasser opretholder invariance (i tilfælde af ændringer i koden)  
  
## Unit test af Person klassen samt domæne primitiver  
I denne opgave skal du implementer unit tests af en klasse samt dens domæne primitiver.  
Gør følgende:  
  
1. opret en ny folder, kald den F.eks. PersonUnitTests.  
2. Fra løsnings repositoriet (uge 40), kopier person klassen og domæne primitiver over i den nye folder.  
_Week40->PersonWithDomainPrimitivesPython_  
3. Opret en test file for hver domæneprimitive samt en test file til Person klassen.  
_Her kan du med fordel genbruge den fil du oprettede i forrige øvelse, men med tilpasset filenavn(skal starte med Test) og klasse navn, F.eks. TestDomainprimitiveAge_  
4. implementer nu en de nøvendige test cases i hver fil, fremgangs måden er:  
      - For hver håndhævelse af invariance i klassen, skal der som minnimum være en test.  
      - sikker dig navenet på hver test case overholder navne konventionen: test_<hvad der testes for>_<forventede resultat>  
      - start med at test domæne primitiverne.  
      - afslut med at test Person klassen.  
      - test kun den kode som er klassens ansvar.  
  
Til sidst skal du reflekter over hvorfor dette øger sikkerheden i en software? er det svære  at lave uhænsigtsmæssige ændringer?  
  
## Opgave til den hurtige  
prøv at implementerer unit tests til Python user klassen fra uge 40.  
    
## Information  

## Instruktioner  

## Links  
[Hvad er cvvs scores](https://www.balbix.com/insights/understanding-cvss-scores/)
  