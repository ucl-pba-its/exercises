# Opgaver Uge 47
   
## Audit 3.parts software  
Når man benytter sig af 3 parts bibloteker/frameworks såsom Flask. Så benytter man sig af kode som er implementeret af en 3 part.
Ligesom i alt andet software kan der opstå(bliver opdaget) sårbarheder i den software.
For at imødegå dette gør man bruge af automatiseret skannings værktøjer, som gennemgår alle de 3 parts afhængningheder 
man har installeret og verificerer om den version man benytter har nogle
sårbarheder (CVE's), dette kaldes en _package audit_. Dette skal der arbejdes med i denne opgave:  
  
1. Klone det oprindelige SOME projekt.  
2. I requirements.txt filen skal du tilføje linjen `pip-audit`  
3. Installer alle afhæningheder, F.eks med kommandoen `pip3 install -r requirements.txt`  
_Evt. i et virtuelt python miljø_  
4. eksekverer kommandoen `pip-audit`  
5. Kopier navnet på den sårbar som vises, og se om du kan finde dens tilsvarende CVE  
  
## Build pipelines  
Der er to opgaver med build pipelines. Den første er en pratisk introduktion til opsætning af en build pipeline med Gitlab.
Den anden opgave introducer konceptet miljø variabler og brugen af disse på operativ systemer(Windows) og ved deployment
med en build pipeline.  
  
### Build pipeline opgave 1.  
læs og udfør opsætning af en build pipeline som beskrevet i [Opgave 1. Pratisk introduktion til CI/CD med Gitlab](pdfs/1.%20Praktisk%20introduktion%20til%20CICD%20med%20Gitlab%20&%20Heroku.pdf)  
  
### Build pipeline opgave 2.  
læs og udfør opsætning med brug af variabler som beskrevet i [Opgave 2. Pratisk introduktion til CI/CD miljø variabler med Gitlab](pdfs/2.Pratisk%20introduktion%20til%20CICD%20miljø%20variabler.pdf)  
  
  
## Information  

## Instruktioner  

## Links  

  