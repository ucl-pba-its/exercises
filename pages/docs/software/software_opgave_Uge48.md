# Opgaver Uge 48
   
## TLS certificate øvelser   
  
**Intentionen med opgaverne er blot at demonstrer https med flask og genereringen af certifikater.**
**Man bør ALRDIG avende self-signed certifikater i produktion.**  
**Generelt bør man overlade certifikater o.l. til http server eller ligende.**
  
Openssl skal bruges til at generer det certifikater med. 
For at installer Openssl på linux, se [her](https://www.openssl.org/)  
For at installer Openssl på windows, installer [chocolatey](https://phoenixnap.com/kb/chocolatey-windows) og eksekver derefter følgende kommando `choco install openssl` i Powershell.
Efter installationen skal Powershell genstartes.  
  
Applikationen som skal bruges til opgaverne med de selv signeret certifikater kan findes i løsnings repositoriet
i folderen _week48->FlaskUsingSelfSignedCertificate_. Alle certifikater kan med fordel oprettes
i den folder.
    
### Opret et selv signeret certifikate.  
De 3 overordnet skridt for at lave et certifikate er:  
  
1.  Generer en privat nøgle  
2.  Lav et _Certificate signing request_ (CSR)
3.  Lav et selv signeret certifikat.
  
#### Genererer privat nøgle.
Eksekver kommandoen `openssl genpkey -out private.key -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -aes-128-cbc`
 
Når nøglen er generet vil indholdet af filen _private.key_ se således ud(indholdet mellem begin og end er dog  anderledes):  
   
-----BEGIN ENCRYPTED PRIVATE KEY-----  
MIIFLTBXBgkqhkiG9w0BBQ0wSjApBgkqhkiG9w0BBQwwHAQIQ9EAjihTKNgCAggA  
MAwGCCqGSIb3DQIJBQAwHQYJYIZIAWUDBAECBBANAsPcspz4RjqazidCiyHbBIIE  
0H0sgjcU5hwt3i0W5Wt7O2kb70VpymNEUMz5dbB1jNVk53MsfFK8LcM4AgaW3I7g  
NbSdE1MMey5Hil+Ru90TClUpJC3uI5JTV83T+c+Cp2SLEF8Po+/EMRMEgUlt4yae  
z9BNnWMcx8iJyrz9z5JBSwN+dD633z4C498kCq+VpKp09kDjYnQP5qwuvMH2nSvN  
V/X84rQ5XUike15Pi7DedquFrgnkNI36zCM1hnAk+688Xko0c8mlavNAtlX0A3z1  
VS9WpDU9+r+dabXCejVsFdpVn3CIOReB8EjsEw5VyJWUNlEnLyfgog/s4WeNrq0c  
T1G3wTHIdlSPZSqFm9im0djp/e7OGqaCwlR9HcMVtax92BYHm7wVT2xXLf492WFS  
5rXG08iX91jpsupj+Wm/8MOueVwGs2eJFUkTNyxmXtNnrx1UAPa+VDrjHJuhy93g  
sPHJiL5WT050TXWDTwX2MnG2sMWuzFhwwcGMvh99WIFR+ecwENH7aN2yTJ04H0u+  
79UD0YA0MjAC/PLGOuu/82fN5kOdTi4QgpKFjciohNnROP2kEfVlYFNYVFWPDnLb  
8Y55VR7le+ybiJ3Jc79FloPpHiagLxgGE9g7AY9jRkBJFaQ4gtfMKLW8X00vAeO/  
ZErClxcvIWU46EyCDPHSwM2WtT7ZeLQ9GyInDsoQxpVu0yrDmRKRuBM7HkZGgy/L  
i84b4DszmE2UDLHoY9YZ+D+76A9rrBF2Z249vwgoBpRe/CYRTSyh5i3jh0EAfzn7  
Z2kaT447yXzotYtC7lCq3+6/H65phB8332w7KvaAkwqbUJuQgmx07m43wC0KO0YP  
jKCRL4Mv2pCMKUOxRec2JJNpItl4VXgj8/CiRCjTNufOANxqfPzxSChXQaPO0Lk7  
3KHAKgD7L27yq9uHFpUpB1iJK3GXfX02vtik8uTImgLs9xHR9J5LrpwfGUR1xH4d  
r9C0vPHs1szRvNmp5eVfEfvcHKyw2+1ASvy+QUaFBJnrOkKL6zgzKmnuhKs37rwq  
pOoas1SIdKJpXuC1DV6Ez11rVWR0gxq9WaBYXJAR01XV4pvvDA6lCIt5inb4bubj  
BKRzX85A7fEEefRzjLJDnsmkH/uLm3T3zEIqvK4KDoldJVG697qhGFYkfw4m1VWv  
xF9TScjE6ZQAljWPSngGRKCCULciSVk43qQAv/bX71/IfNaVMy4JNP4ICdS2WbKl  
c3OmMeNtlUcOavFtl/UwsBnEB3B9UtcD5DYeA5+6YUs9kyZYxCBuOcKRKF8vbNLh  
13Dl3IZXMOPPY1lDHKvRVYzc2ovnahmTXK1lTyXEx05oeOodLRcG+AXgzmsz14mC  
w2RJNI4SBe+y6SApciekUJ7Igroa9ZGzg3HyGbOXkIFySYv9xPFoEgCcbaVuF6Kl  
6EY+WeAYiy6py8yXQ6DWpfiBXhKTQvTJn0dcFP3pX8yODaf1126Q2dJU215Au1NP  
S+7Uy4cB5gcIz0/+CRNta+/0osmfyyBeOzvhIOm4JtGFEPwHehOo7Gk9Xkn2mzyX  
7tblSwDf675NBZbTlAwyH2oiP+hy0mUQCFymmV3Mgm8+X2Fpe4J7kH7qWD+oo8Wh  
PEOnVQp6Vgo8bE+/+D12gwWWumqyKgztYLgoHEJyrXYZ  
-----END ENCRYPTED PRIVATE KEY---  
  
#### Lav _Certificate signing request_  
Eksekver kommandoen `openssl req -new -key private.key -out signingRequest.csr`  
- flaget _new_ specificeres at der skal oprettes en ny CSR  
- flaget _key_ specificere den privat nøgle CSR skal benytte sig af (private nøglen til det certifikat som bliver signeret med CSR).  
- flaget _out_ specifere hvilken file CSR skal oprettes i.  
  
Når kommandoen bliver eksekveret, anmodes der i konsolen om noget data.  
Her er det vigtig at common name passer med host navnet, F.eks.  
Hvis en side hostes på domænet _www.navn.dk_ skal common name afspejle dette.

#### Lav et selv signeret certifikat.
Eksekver kommandoen `openssl x509 -req -days 365 -in signingRequest.csr -signkey private.key -out certificate.crt`  
  
#### Konventer crt til PEM format  
Flask benytter sig af certifikater i formattet pem. Derfor skal de selv signeret certifikat nu konventeres til pem format.  
Eksekver kommandoen `openssl x509 -in certificate.crt -out certificate.pem`
  
#### Konventer private nøgle til PEM format  
Eksekver kommandoen `openssl rsa -in private.key -out private.pem`  

#### Eksekver applikationen.  
Du kan nu eksekver applikationen.
fordi certifikatet ikke er signeret af en CA vil der komme en advarsel, hvilket er betyder at 
certifikatet ikke er gyldigt ift. at autentificerer sig selv. Men hvis du roder lidt med det i 
wireshark, vil du kunne se at krypteringen virker. Du kan se i applikationens readme hvordan du eksekver applikationen.
  
## Hashing øvelser  
  
De følgende opgaver introducerer brugen af hashing bibloteker i Pyhton.

### SHA256 hashing
SHA256 kan i Python bruges uden at skulle importer 3. parts bibloteker via pip.  
Det kommer med standard bibloteket hashlib. I denne opgave skal du benytte hashlib SHA256 hash.  

1. Lav en SHA256 hash applikation. Tutorialen [her](https://datagy.io/python-sha256/) viser  hvordan dette kan gøres.  
_Vær opmærksom på hvor i koden teksten bliver konventeret bytes, dette skal du vide når du senere skal salte dit hash_  
  
Som nævnt i dagens oplæg, vil SHA256 altid lave samme hash, udfra samme tekst. Dette kan der dog ændres på
ved brug af _salt_. I næste opgave skal du salte inden du hasher.
  
1. I applikationen fra forrige opgave skal du nu importer biblokteket `os` (stadig ikke brug for pip)  
2. Du skal lave et salt ved at kalde metoden `urandom` med parameteret `4` på det importeret `os` objekt.
_urandom bruger operativ systemet til at lave en kryptografisk sikker pseudo tilfældig værdi_
3. Inden du hasher din tekst skal du samtræk teksten med dit salt. (teksten skal være i byte format)
4. Udskriv herefter den saltet hash værdi. Kør applikationen flere gange for at bekræfte at der kommer en ny hash værdi hver gang .
  
### BCrypt
I denne opgave skal du benytte bcrypt til at lave hash med.  
Implementer bcrypt hashing som vist [her](https://zetcode.com/python/bcrypt/) i afsnittet _Python bcrypt create hashed password_  
_I denne opgave skal du bruge pip til at importer 3.parts bibloteket bcrypt_  


## Information  

## Instruktioner  

## Links  

  