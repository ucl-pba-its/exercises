# Opgave 11 - Særlige rettigheder i Linux.

## Information
Linux understøtter 3 særlige rettigheder benævnt SID,GID og sticky bit.

|Navn                     |Numerisk værdi|karakter  |
|-------------------------|--------------|----------|
|User ID bit              |4             |u+s       |
|Group ID bit             |2             |g+s       |
|Sticky bit               |1             |o+t       |  

De særlige rettigheder kan tildeles ved at angive 4 decimale frem for 3.
Altså vil jeg tildele den særlige rettighed _user ID bit_ kan dette gøres
med kommandoen `chmod 4700`.i dette tilfælde vil User ID bit være sat, og 
ejeren af filen vil have læse,skrive og eksekverings rettigheder. Gruppen og
alle andre vil ingen rettigheder havde. 
![user id bit](./Images/UserIdBit.jpg)  
s i rettigheden fortæller at  det særlige bit er sat til User id bit da den erstatter x.

### Sticky bit
Sticky bit bruges til at give skrive rettigheder, men fjerne muligheden for at sletning.
Altså når sticky bit er sat kan der ændres, men ikke slettes.
### Group id bit
Når group id er er sat på et directory, bliver alle filer i directoriet automatisk tildelt
samme  rettigheder som directory gruppen har.
### User id bit
User id bit tillader at der eksekveres med ejerens rettigheder. dog kun ved binærer filer.

I denne opgave vil vi udnytte en sårbarhed som User id bit åbner op for.

## Instruktioner
1. hent applikations filen martins_very_non_suspicous_app på Its learning, og unzip den.
2. sørg for at filen er på din linux VM.
3. sæt root brugeren som ejer af filen.
4. giv root brugeren alle rettigheder til filen og sæt User ID bit. Gruppen og alle andre skal blot havde eksekverings rettigheder.
5. Skift til din almindelig bruger.
6. ekserkver applikationen.
7. skriv `whoami`

Tanken med øvelsen er at vise en typisk angrebs model ift. til et _Escalation of privliges_ angreb. Og hvilken 
sårbarheder SID og GID potentielt åbner op for. 


   
## Links

  
