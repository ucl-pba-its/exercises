# Opgave 13 - rsyslog 

## Information
 De fleste Linux distrubutioner idag har to daemons(applikations processer) som logger parralelt
 med hinanden. Der er rlogsysd, og journalctld. Den pratiske forskel der er på de 2 log daemons, er at
 rlogsysd logger i plain text filer, hvorimod journalctld logger i binær filer.

I faget system sikkkerhed arbejdes der primært med rsyslog. 


## Instruktioner

### Opsætning af locate til søgning.
kommandoen `find` er god til søgning af filer, men `locate` kan også
med fordel anvendes.

1. installer locate med kommandoen `sudo apt install locate`
2. Opdater "Files on disk" databasen, `sudo updatedb`

### Dan overblik over Rsyslog logfilerne på operativ systemet.

1. Brug `locate` til at finde alle filer med ordet `rsyslog`
2. Dan dig et generelt overblik over filerne. Er der mange tilknyttet filer? 

### Rsyslog konfigurations file.
Rsyslog log konfiguration filen indeholder den generelle opsætningen af rsyslog daemon,
blandt andet hvem der ejer log filerne, og hvilken gruppe der er tilknyttet log filerne.
Herudover har den module opsætning. Moduler er ekstra funktionalitet som man kan give til
rlogsys.

1. brug locate til at finde rsyslog filen `rsyslog.conf`
2. åben filen med `nano`
3. I konfigurations filen, find afsnittet `Set the default permissions for all log files`.
4. Noter hvem der er file ejer, og hvilken gruppe log filerne er tilknyttet.
5. Udforsk de andre områder af filen.

## Links

  
