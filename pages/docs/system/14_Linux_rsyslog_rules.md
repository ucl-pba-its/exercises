# Opgave 14 - 
Logging regler for rsyslog kan ændres 
## Information
Reglerne for hvad rsyslog skal logge, findes i  filen `50-default.conf`.

Reglerne for skrives op som _facility.priority action_

Facility er programmet der bliver logget fra F.eks. _mail_ eller _kern_.
Priority fortæller system hvilket types besked der skal logges. Beskeder
typer identificerer hvilken priortering den enkelte log har. Nedenunder
er der en besked typer listet i priorteret rækkefølge. Højeste priortet
først.

1. emerg.
2. alert.
3. crit.
4. err.
5. warning.
6. notice.
7. info.
8. debug.  
  
Altså log beskeder af typen emerg  er vigtige beskeder der bør reageres på
med det samme. Hvor imod debug beskeder er mindre vigtige.
_Priority_ kan udskiftes med wildcard `*`, hvilket betyder at alle besked
typer skal sendes til den file som er defineret i _action_

## Instruktioner

1. i ryslog directory, skal filen `50-default.conf` findes.
2. Åben filen `50-deault.conf`
3. Dan et overblik over alle de log filer som der bliver sendt beskeder til.
4. Noter hvilken filer mail applikationen sender log beskeder til, ved priorteringen _info_ , _warn_ og _err_

## Links

  
