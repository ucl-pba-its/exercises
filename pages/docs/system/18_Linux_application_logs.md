# Opgave 18 - Applikations logs

## Information
I denne opgave skal der arbejde med applikations logs.
 
## Instruktioner
1. installer apache2 web server med kommandoen  `sudo apt-get install apache2`
2. Verificer at servicen er startet med kommandoen `systemctl status apache2`
3. I en browser, gå ind på url'en <VM IP  adresse>. Verificer at det er apache's default side der kommer frem
4. Udskriv indholdet af apache's adgangs log som findes i  `/var/log/apache2/access.log`
5. Verificerer at den sidste log  i filen viser at den sidste maskine som har tilgået web serveren er den som du brugte til at udføre _trin 3_
6. Eksekver kommandoen `tail -f access.log`
7. Udfør _trin 3_ et par gange igen, og verificer at der kommer en ny log entry hvergang.

Mange af de tidligere opgaver har været centeret om log filer fra operativ systemet. Men det er meget
vigtig at huske, at applikationer ofte har sine egene log filer (og autentificering). Og at hver applikation eksekveres med et sæt bruger rettigheder.

## Links

  
