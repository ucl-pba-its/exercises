# Opgave 19 - Installering af iptables

## Information
Der er flere forskellige firewall man kan benytte på Linux. Den mest udbredte dog nok (foreløbigt)
iptables. Denne skal vi arbejde med idag.
 
## Instruktioner
1. Installer iptables med kommandoen `sudo apt install iptables`
2. Udskriv version nummeret med kommandoen `sudo iptables -V`
3. Installer iptables-persistent med kommandoen `sudo apt install iptables-persistent`
4. Udskriv alle firewall regler(Regel kæden) med kommandoen `sudo iptables -L`
5. Reflekter over hvad der menes med firewall regler, samt hvad der menes med "regel kæden".

**Når man har ændret en iptables firewall regel, skal man manuelt gemme den nuværende regel opsætning med kommandoen `sudo netfilter-persistent save` ellers er regel listen tom efter genstart**


## Links

  
