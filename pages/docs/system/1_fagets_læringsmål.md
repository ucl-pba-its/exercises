# Opgave 1 - Fagets Læringsmål

## Information

Når vi påbegynder et nyt projekt eller arbejdesopgave er det vigtig at først forstå det overordnet formål.
Det samme gør sig gældende som studerende når man starter et nyt fag. Altså hvad forventes der at den studerende
kan når faget afsluttes. Dette giver samtid den studerende mulighed for at afgrænse sig, da et 10 ECTS fag indenfor IT-sikkerhed
til tider har en stor faglig brede. Hertil følger at de studieordninger som beskriver de enkelte fag samt deres læringsmål, ofte er 
skrevet abstrakt og ukronkret, således at den enkelte studerende selv skal tolke hvad der menes, og selv sætte den i kontekst ift.
den konkrette undervisning. Dette kan skabe stor usikkerhed, og tvivl ift. hvad den studerende skal fokuserer på i sit arbejde
udenfor øvelsestimerne som kun udgøre ca. 24 % af det forventet arbejde i faget.

I de følgende opgaver skal i arbejde med jeres forståelse af studieordningens læringsmål.
Som tidligere nævnt kan læringsmålene være ukonkrette og abstrakte, hvilket betyder at læringsmålene
tolkes individuelt. Derfor at det vigtig at i arbejder med dem i jeres teams, og at alle medlemmer af
teamet beskriver deres fortolkning af læringsmålene, således at forståelsen bliver så nuanceret som
muligt. Derfor er det vigtigt at teamet anvender CL strukturen i de følgende øvelser. 


## Instruktioner

1. Læs og refleketer over læringsmålene individuelt  
_Tids estimate: 15 minutter_  
Hvert team medlem skal individuelt læse og reflekter over studieordningens læringsmål for faget [System sikkerhed](https://esdhweb.ucl.dk/D22-1980440.pdf?_ga=2.115552518.1057771612.1675433091-1226966722.1675433091). 
2. Noter individuelt et konkret eksempel på hvert læringsmål.  
_Tids estimate: 15 minutter_  
Hvert team medlem noter individuelt et konkret eksempel som taler ind til hvert læringsmål.
_F.eks. læringsmålet Sikkerhedadministration i Database management system -> Credentials for hver database_   
_Som 1.Semester studerende kan dette være meget svært, men der er  ikke noget  rigtig og forkert, blot hvad du allerede ved, eller kan  søge dig til_
3. lav en fælles forståelse af læringsmålene.  
_Tids estimate: 30 minutter_  
Benyt CL strukturen [Møde på midten](../Cooperative%20Learning%20Strukturer/Møde%20på%20midten.docx) til at udarbejde en fælles formulering
af  konkrette eksempler som taler ind til hvert læringsmål(gerne flere konkrete eksempler for hver læringsmål).  
_Punkt 1 & 2 i møde på midten strukturen blev opfyldt i forrige opgave_  
  
4.  **Efter pausen** Noter læringsmål med konkret eksemepl i padlet.  
_Tids estimate: 10 minutter_  
Teamet skal nu skrive læringsmålene og de konkrette eksempler ind i den fælles padlet.

Herefter laver vi en fælles opsamling, hvor hvert team præsenterer deres konkrette eksempler.
   

## Links
[National Studieordning](https://esdhweb.ucl.dk/D22-1980440.pdf?_ga=2.115552518.1057771612.1675433091-1226966722.1675433091)