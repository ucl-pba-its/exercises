# Opgave 20 - Bloker alt indagående trafik

## Information
En firewall giver mulighed for at filtre på mange måder. Som udgangspunkt giver den mulighed for at åbne
op for kommunikation på specifikke netværks porte, eller lukke ned for kommunikation på specifikke netværks
porte. Som udgangspunkt bør man lukke for kommunikation på alle porte, og så herefter åbne for de specifikke 
porte der er behov for.
  
Firewall regler behandles ofte en ad gangen, i rækkefølge. Dette kalder man en regel kæde.
Reglerne i en regel kæde, håndhæves ofte i kronlogisk rækkefølge, F.eks.
  
Regel 1: Tillad TCP Forbindelser på port 80.  
Regel 2: Forbyd alle UDP forbindelser.  
Regel 3: Forbyd Alle TCP forbindelser.  
Regel 4: Tillad UDP forbindelser på port 53.  
  
I overstående tilfælde kommer regel 1 før regel 3. Og derfor tillader firewallen at 
der oprettes TCP forbindelser på port 80. Omvendt kommer regel 2 før regel 4 og derfor 
er **UDP forbindelser på port 53 ikke tilladt**.
Bruger man `-A` muligheden til at indsætte en regel bliver den indsat til sidst i regelkæden. Når
firewall reglerne eksekveres kronologisk, bør en _drop alt trafik_ regel altid komme til 
sidst i regel kæden.
 
## Instruktioner
1. Udskriv regel kæden, og noter dig hvordan den ser ud. (Regel kæden er tidligere blevet udskrevet i opgave 19, trin 4)
2. Lav en regel i slutningen af regelkæden som dropper alt trafik med kommandoen `sudo iptables -A INPUT -j DROP` 
3. Udskriv regel kæden igen, og noter forskellen fra trin 1.
  
### Bonus opgave  
Hent applikationen [zenmap(GUI udgave af nmap)](https://nmap.org/zenmap/) ned på din lokale pc (Ikke VM),
og scan din VM før og efter du har tilføjet reglen om at bloker alt trafik.

## Links

  
