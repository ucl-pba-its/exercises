# Opgave 23 - Tillad indadgående trafik fra specifikke ICMP beskeder.

## Information
ICMP pakker bliver brugt til såkaldte "ping request", og anvendes ofte af angriber i rekognoscerings fasen
hvor netværket bliver skannet. Man kan skjule sin host ved at blokerer for ICMP pakker. At bloker ICMP pakker
er ikke i sig selv en foranstaltning mod et angreb, men det besværliggøre dog angriberens arbejde en smule.
Nogle enkelte typer af ICMP pakker er dog belejlige at kunne modtage.   
I opgaven skal der 3 typer af ICMP pakker tillades.  
 
## Instruktioner
1. tillad ICMP pakker af type 3 med kommandoen  `sudo iptables -A INPUT -m conntrack -p icmp --icmp-type 3 --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT`
2. tillad ICMP pakker af type 11 med kommandoen  `sudo iptables -A INPUT -m conntrack -p icmp --icmp-type 11 --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT`
3. tillad ICMP pakker af type 12 med kommandoen  `sudo iptables -A INPUT -m conntrack -p icmp --icmp-type 12 --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT`
4. De ICMP pakker der er blevet tilladt, er hver især af en bestemt type. Undersøg hvad hver af de 3 typer betyder.

## Links

  
