# Opgave 25 - Tillad indagående trafik fra en specifik ip adresse.

## Information
For at anvende _allow list_ konceptet kan man vælge at give specifikke ip adresser lov til at kommunikerer med hosten på
en specifik port. I denne opgave skal der åbnes op for kommunikation med en specifik ip adresse.
  
_her kan der med fordele åbnes for din lokal pc's ip adresse_  
_Du kan teste kommunikationen med apache serveren der blev sat op i opgave 18_
  
## Instruktioner
1. Lav en regel der tillader modtagelse af beskeder fra en ip adresse med kommandoen `sudo iptables -A INPUT -p tcp -s <Source IP> --dport 22 -m conntrack --ctstate NEW -j ACCEPT`
2. Lav en regel der tillader afsendelse af beskeder til en specifik ip adresse med kommandoen `sudo iptables -A OUTPUT -p tcp --dport 53 -m conntrack --ctstate NEW -j ACCEPT`

## Links

  
