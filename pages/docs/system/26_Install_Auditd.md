# Opgave 26 - Installer auditd

## Information
Audit daemon (auditd) er Linux's audit system. Auditd kan overvåge jvf. tilladelser, altså Read,Write & Excute.
Typisk anvendes audit systemet til overvågning af filer, directories & system kald(Kald til OS API'et).

Audit daemon fungere principielt som et logging system for  prædefineret begivenheder. Alle begivenheder 
defineres som `Audit regler`. Et eksempel på et prædefineret begivenhed 
kunne være ændringer i en bestemt file. Hver gang der bliver ændret i filen, udløser dette en begivenhed som
resulter i et `Audit log`. En slags "alarm" der udløses af en konkret begivenhed.
Som udgangspunkt kan alle `auditd` logs findes i `/var/log/audit/audit.log`. 
  
Generelt er auditd  ikke særligt intuitivt at arbejde med, men det skaber en god forståelse for hvordan 
et sådan system virker.
  
**Vigtig! auditd er ikke version styring, og holder derfor ikke styr på hvad der blev ændret**  
audit systemet kan holde øje med hvilken bruger der har ændret hvilken filer, directory eller foretaget system kald.
Men ikke hvad der blev ændret.
  
## Instruktioner
1. Ubuntu har som udgangspunkt ikke audit daemon installeret. Installer audit daemon med kommandoen `apt install auditd`
2. Verificer at auditd er aktiv med kommandoen `systemctl status auditd`.
![Auditd active](./Images/AuditDActive.jpg)
3. brug værktøjet `auditctl` til at udskrive nuværende regler med kommandoen`auditctl -l`
![No rules auditd](./Images/NoRulesAuditd.jpg)
4. Udskriv log filen som findes i `/var/log/audit/audit.log`
![auditd lof](./Images/AuditdLog.jpg)  
  
Hvis loggen virker uoverskuelig, har du det tilfælles med næsten resten af verden. Derfor bruger man 
typisk to værktøjer benævnt `ausearch` og `aureport` til at udlæse konkrette begivenheder. Begge værktøjer
arbejdes der med i de kommende opgaver.

At arbejde direkte med auditd er en smule primitivt ift. til mange andre værktøjer. Men det giver en forståelse
for hvordan der arbejdes med audit på (næsten) laveste nivevau i Linux. Mange større og mere intuitive audit systemer
(F.eks. SIEM systemer) benytter sig også af auditd, eller kan benytte sig auditd loggen.


## Links
[auditd man page](https://linux.die.net/man/8/auditd)
  
