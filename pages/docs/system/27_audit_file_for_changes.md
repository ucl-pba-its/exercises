# Opgave 27 - audit en file for ændringer

## Information
For at definerer begivenheder som udløser en "audit log" skal man opsætte en audit regel.
 Enten ved at tilføje reglen via værktøjet `auditctl` eller at tilføje en regel i audit regel filen `/etc/audit/audit.rules`  
 _At bruge loogen direkte er dog en smule uoverskueligt_
  
## Instruktioner

### Tilføj audit regel med auditctl`
1\. Opret en audit regel for hver gang der skrives eller ændres på filen `/etc/passwd` med kommandoen
`auditctl -w /etc/passwd -p wa -k user_change`  
  
_Muligheden `-w` står for `where` og referer til den file hvor audit reglen skal
gælde. `-p` står for `permission` og fortæller hvilken rettigheder der skal overvåges.
I eksemplet står der `wa` hvilket står for attribute og write. Hvis filens metadata ændres, eller 
der skrives til filen, udløser det en begivenhed og der bliver oprettet et audit log_  
  
2\. udskriv en rapport over alle hændelser der er logget med kommandoen `aureport -i -k | grep user_change`  
   
_Muligheden `-i` står for intepret, og betyder at numerisk entiter såsom bruger id bliver fortolket som brugernavn. `-k` står  for key, hvilket betyder at regel der udløst audit loggen skal vises_  
   
3\. tilføj noget text i bunden af filen fra trin 1.  
4\. Udfør trin 2 igen, og bekræft at der nu er kommet en ny række i rapporten  
   _Der kommer en del nye rækker_  
5\. Brug kommandoen `ausearch -i -k user_change`. Her skulle du gerne kunne finde en log som  ligner den nedstående.  
   ![Ausearch](./Images/ausearchforfileChange.jpg)  
Generelt kan det være en smule rodet at skulle finde rundt i disse logs. Oftest bliver der produceret mere end en
log når en begivenhed udløser en regel. Forskellen på `ausearch` og `aureport` er at ausearch viser flere oplysninger, men dermed også bliver mere uoverskuelig.


### Tilføj audit regel med audit regel konfigurations file
Ulempen ved at tilføje regler direkte med auditctl er at regler ikke bliver persisteret. Altså de bliver ikke gemt
og forsvinder når auditd eller hele systemet bliver genstartet. For at gemme reglerne skal man lave en regel file
og gemme den i `/etc/audit/rules.d/`

1\. Åben filen `/etc/audit/audit.rules`. Den skulle gerne se ud som vist nedenunder.
![Audit rule file](./Images/AuditRuleFile.jpg)  
_regel filen bliver indlæst ved opstart af auditd og alle regler heri bliver de gældende audit regler_  
  
Dette er auditd's primær konfigurations file, som indeholder alle regel konfigurationerne. men bemærk kommentaren øverst i filen. Den fortælle at denne file bliver autogeneret ud fra indholdet af `/etc/audit/audit.d` directory (ikke en synderlig intuitiv måde at lave konfigurations opsætning på).  
2\. Opret en ny file som indeholder reglen, med kommandoen `sh -c "auditctl -l > /etc/audit/rules.d/custom.rules"`  
_**For at kommandoen virker skal reglen der blev lavet trin 1 forrige afsnit stadig være gældende, kontroller evt. med `auditctl -l inden trin 2 udføres`**_  
3\. Genstart auditd med kommandoen `systemctl restart auditd`  
4\. Udskriv indholdet af `/etc/audit/audit.rules`. Resultatet bør ligne det som vises på billedet nedenunder.  
![Audit config file with rule](./Images/AuditConfigFileWithRule.jpg)     
  
**Slet filen /etc/audit/rules.d/custom.rules efter øvelsen**

### Audit alle file  ændringer.
Man kan  også udskrive alle ændringer der er blevet lavet på overvåget filer.

1. Eksekver kommandoen `aureport -f`. Med denne bør du få en udskrift der ligner nedstående.  
![All files audit](./Images/AllFileAudits.jpg)  
  
Den første række med nummeret 1. blev indsat 27 Marts 2023. kl. 20:11. begivenheden som udløste loggen
var en udskrift af filen tmp.txt. udskrivning blev lavet af brugeren med id 1000. Og udskrivningen blev eksekveret med 
kommandoen  `cat`  
2. Ændre kommandoen fra trin 1 således den skriver  brugernavn i stedet for bruger id



## Links
[audit log file system](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/security_guide/sec-understanding_audit_log_files)  
[openat dusystem kald](https://manpages.ubuntu.com/manpages/xenial/man2/open.2freebsd.html)  
[aureport man page](https://man7.org/linux/man-pages/man8/aureport.8.html)

  
