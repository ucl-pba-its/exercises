# Opgave 28 - audit et directory

## Information
Directories kan også overvåges med en audit log. Fremgangs måden er den samme som
med filer.  
  
De rettigheder man monitorer på (Read,write,attribute og execute) fungere på samme måde.
Execute bliver udført når nogen prøver at skife sti ind i directoriet, F.eks. `cd /etc/` 
  
## Instruktioner
1. Opret et nyt directory 
2. Opret en audit regel med kommandoen `auditctl -w <Directory path> -k directory_watch_rule`  
_Bemærk at permission er bevist undladt_ 
3. Brug auditctl til at udskrive reglen. Bemærk hvilken rettigheder der overvåges på. Dette er pga.´-p´ muligheden blev undladt.
4. Brug `chown` til at give `root` ejerskab over directory(fra trin 1), og brug `chmod` til at give root fuld rettighed og alle andre  skal ingen  rettigheder  havde.
5. Med en bruger som ikke er root, eksekver kommandoen `ls <Directory path>` (directory er fra trin 1)
6. eksekver kommandoen `ausearch -i -k directory_watch_rule`. Dette resulter i en log som ligner nedstående.
![permission denied](./Images/lsPermissionDenied.jpg)  
  
ausearch er mindre læsbart end aureport, men indeholder tilgengæld mere information.
Af en for mig ukendt årsag kan aureport ikke bruges til directory regler.
Hvis man skal gennemgå større audit log filer hvor man leder efter noget specifikt,
så kan `grep` kommandoen hjælpe med at filtre.


## Links
[ausearch man page](https://man7.org/linux/man-pages/man8/ausearch.8.html)
  
