# Opgave 29 - audit OS api'et for specifikke system kald

## Information
Man kan monitorer alt kommunikation mellem kørende processer(applikationer) og operativ systemet.
Dette gøres ved at monitorer `system kald`.  

  
## Instruktioner

### monitorer processer som bliver slukket
1. Tilføj reglen for `auditctl -a always,exit -F arch=b64 -S kill -F key=kill_rule `
2. start en baggrunds proces med kommandoen `sleep 600 &`
3. find proces id'et med `ps aux` på sleep processen
![Sleep proces](./Images/ProcesToKill.jpg)
4. dræb processen med kommandoen `kill <proces id>`
5. Eksekver kommandoen `aureport -i -k | grep kill_rule` og verificer at der er lavet nye rækker.  
_Formodentlig en del nye rækker, auditd har ofte sit eget liv_

## Links

  
