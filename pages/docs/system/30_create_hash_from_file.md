# Opgave 30 - checksum fra file

## Information
For at sikre integriteten af data benytter man ofte en hash værdi som checksum.
Man kan lave checksum af en enkelt file, eller afbilledinger af hele lageringsmedier.

En hash funktion bruges til at lave checksummer med. Hvis blot et enkelt karakter
ændres i en hash funktions input, bliver outputtet helt anderledes.

En checksum sikre integriteten på data. Hvis en checksum har ændret sig, har 
data'en også ændret sig.
  
## Instruktioner
1. Installer `hashalot` med `apt`.
2. Opret en file som indeholder teksten `Hej med dig`.
3. Lav en checksum af filen med kommandoen `sha256sum <path to file>`.
4. Lav endnu en checksum af filen, og verificer at check summen er den samme.
5. Tilføj et `f` til teksten i filen.
6. Lav igen en checksum af filen.
7. Verificer at checksum nu er en helt anden.

## Links
[hashalot man page](https://manpages.org/hashalot)
  
