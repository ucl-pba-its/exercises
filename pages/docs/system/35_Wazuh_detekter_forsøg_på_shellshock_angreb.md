# Opgave 35 - Detekter forsøg på shellshock angreb

## Information
I forrige opgave (34) overvåget wazuh apache web serveren for forsøg på sql injection angreb.
Wazuh kan også "ud af boksen" detekter andre angreb. I denne opgave er det angrebet "Shellshock" som 
skal detekteres. Denne gang forsøges den ondsindet kode at "injected" gennem headeren _User-Agent_.





## Instruktioner

### Opsætning af Wazuh agent
1. Installer apache og overvåg loggen (Såfremt du ikke allerede har gjort dette i opgave 34)

### Udfør angreb
1. Fra den angribende host, eksekver kommandoen `sudo curl -H "User-Agent: () { :; }; /bin/cat /etc/passwd" <Den overvåget host-IP>`

### Wazuh dashboard
1. Gå til Security events.  
2. filterer på: `rule.description:Shellshock attack detected`  
![Shellshock attack detected](./Images/Wazuh_Dashboard/Shellshock_inject_attempt.jpg)
   

## Links
[Detecting a Shellshock attack](https://documentation.wazuh.com/current/proof-of-concept-guide/detect-web-attack-shellshock.html)
