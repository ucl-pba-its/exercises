# Opgave 3 - Navigation i Linux filesystem med bash

## Information
Formålet med denne  opgave er at introducerer de grundlæggende linux kommandoer.

I opgaven skal der eksperimenteres med Linux CLI kommandoer i BASH shell.
Fremgangsmåden er at du bliver bedt om at eksekverer en kommando, og herefter noter
hvad der sker. **Målet med disse øvelser** er at du skal bygge et _Cheat sheet_ med linux kommandoer i dit gitlab repositorier, og få en genral rutinering med grundlæggende Linux Bash kommandoer. **Det betyder følgende for alle trin i opgaven:**

1. Udfør kommandoen.
2. Observer resultatet, og noter det herefter i dit _Cheat sheet_
**_Altså efter hver eksekveret kommando, skal du kunne redegøre for hvad den gjorde_**

## Links til beskrivelse af kommandoerne i opgaven:  
[pwd](https://man7.org/linux/man-pages/man1/pwd.1.html)  
[cd](https://man7.org/linux/man-pages/man1/cd.1p.html)  
[ls](https://man7.org/linux/man-pages/man1/ls.1.html)  
[touch](https://man7.org/linux/man-pages/man1/touch.1.html)  
[mkdir](https://man7.org/linux/man-pages/man1/mkdir.1.html)  

## Instruktioner
1. eksekver kommandoen `pwd`
2. eksekver kommandoen `cd ..`
3. eksekver kommandoen `cd /`
4. Lokationen `/` har en bestemt betydning i Linux file systemet. Hvad er det? (Her kan du med fordel søge svar med Google)
5. eksekver kommandoen `cd /etc/ca-certificates/`
6. Hvor mange _Directories_ viser outputtet fra `pwd` kommandoen nu?
7. eksekver kommandoen `cd ../..`
8. Hvor mange _Directories_ viser outputtet fra `pwd` kommandoen nu?
9. eksekver kommandoen `cd ~` (Karakteren hedder tilde. I Ubuntu kan den nogen gange findes med knappen F6)
10. Kommandoen `~` er en "Genvej" i linux, hvad er det en genvej til?
11. i file systemets rod(/), esekver kommandoen `ls`
12. i brugerens _Home directory_, eksekver kommandoen `touch helloworld.txt`
13. i brugerens _Home directory_, eksekver kommandoen `touch .helloworld.txt`
14. list alle filer og mapper i brugerens _Home directory_
15. list alle filer og mapper i brugerens _Home directory_ med flaget `-a`
16. list alle filer og mapper i brugerens _Home directory_ med flaget `-l`
17. list alle filer og mapper i brugerens _Home directory_ med flaget `-la`
18. i brugerens _Home directory_, eksekver kommandoen `mkdir helloWorld`
19. Eksekver kommandoen `ls -d */`
20. Eksekver kommandoenn `ls -f`



## Test dig selv  
_Udfør den følgende opgave, med så lidt hjælp som muligt_  
Opret directoriet _minefiler_ i _Home directory_. i minefiler skal der oprettes en file ved navn min `fil1`,   og en skjult file ved navn `skjultfil1`. Udfør herefter følgende:  
  
1. Udskriv en liste til konsolen som viser alle ikke skjulte filer.  
2. Udskriv en liste til konsolen som viser alle filer (Inklusiv skjulte filer)  
3. Gå tilbage til _Home directory_.  
4. Lav et skjult directory ved navn `skjult` (hvordan symboliseret vi tidligere skjulte filer?)  


## Links
[Basic Linux commands](https://www.hostinger.com/tutorials/linux-commands)  
[Basic linux commands videos](https://www.youtube.com/watch?v=gd7BXuUQ91w)
