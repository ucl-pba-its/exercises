# Opgave 4 - Linux file struktur

## Information
File strukteren i Linux ser ud som følger:  
/  
|_bin  
|_boot  
|_dev  
|_etc  
|_home  
|_media  
|_lib  
|_mnt  
|_misc  
|_proc  
|_opt  
|_sbin  
|_root  
|_tmp  
|_tmp  
|_usr  
|_var  

Hver _directory_ har pr. konvention et specifikt formål, altså hvad indeholder de. Det skal der 
arbejdes med i denne opgave, først individuelt og siden i teamet. 

## Instruktioner
1. **Individuelt** undersøg og noter i linux cheats sheet hvad det er for nogle filer som er tiltænkt hver enkelt directory.
_Tids estimat: 25 minutter_
2. **I teamet** Teamet skal nu samligne resultater, og lave en fælles konklusion af hvad hvert enkelt _directory_ er tiltænkt af indeholde. 
_Tids estimat: 10 minutter_

## Links
[Basic Linux commands](https://www.hostinger.com/tutorials/linux-commands)  
[Basic linux commands videos](https://www.youtube.com/watch?v=gd7BXuUQ91w)

