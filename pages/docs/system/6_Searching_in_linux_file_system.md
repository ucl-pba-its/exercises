# Opgave 6 - Søgning i Linux file strukturen

## Information
Formålet med denne  opgave er at introducerer de grundlæggende linux kommandoer.

I opgaven skal der eksperimenteres med Linux CLI kommandoer i BASH shell.
Fremgangsmåden er at du bliver bedt om at eksekverer en kommando, og herefter noter
hvad der sker. **Målet med disse øvelser** er at du skal bygge et _Cheat sheet_ med linux kommandoer i dit gitlab repositorier, og få en genral rutinering med grundlæggende Linux Bash kommandoer. **Det betyder følgende for alle trin i opgaven:**

1. Udfør kommandoen.
2. Observer resultatet, og noter det herefter i dit _Cheat sheet_
**_Altså efter hver eksekveret kommando, skal du kunne redegøre for hvad den gjorde_**

## Links til beskrivelse af kommandoerne i opgaven:  
[find](https://man7.org/linux/man-pages/man1/find.1.html)

## Instruktioner
1. I `Home directory`, eksekver kommandoen `find`
2. I `Home directory`, eksekver kommandoen `find /etc/`
3. eksekver kommandoen `sudo find /etc/ -name passwd` _sudo foran kommandoen betyder at kommandoen eksekveres med de rettigheder som sudo gruppen har_
4. eksekver kommandoen `sudo find /etc/ -name pasSwd` **Husk stort S **  
5. eksekver kommandoen `sudo find /etc/ -iname pasSwd` **Husk stort S **
6. eksekver kommandoen `sudo find /etc/ -name pass*`  
   
_De næste kommandoer bruges til at finde filer baseret på deres byte størrelse. De to første trin bruges blot til at generer de to filer som skal findes_  
  
1. I `Home directory`, eksekver kommandoen `truncate -s 6M filelargerthanfivemegabyte`  
2. I `Home directory`, eksekver kommandoen `truncate -s 4M filelessthanfivemegabyte`  
3. I roden(/), eksekver kommandoen `find /home -size +5M`  
4.  I roden(/), eksekver kommandoen `find /home -size -5M`  
5.  I `Home directory`, opret to _directories_. et der hedder `test` og et andet som hedder `test2`  
6.  I `test2`, skal der oprettes en file som hedder `test`  
7.  I `Home directory`, eksekver kommandoen `find -type f -name test`  
8.  I `Home directory`, eksekver kommandoen `find -type d -name test`  
  
## Links
[Basic Linux commands](https://www.hostinger.com/tutorials/linux-commands)  
[Basic linux commands videos](https://www.youtube.com/watch?v=gd7BXuUQ91w)
 
