# Opgave 8 - Processer og services i Linux

## Information
Som nævnt i dagens forberedelse, er en af egenskaberne ved et operativ system, at der kan ekseverers mange applikationer på 
samme tid. I Linux referer man til kørende applikationer som _process_. Dette kigger  vi på i de følgende opgaver.  

**Forsæt med at noter i dit Linux cheat sheet**

## Links til beskrivelse af kommandoerne i opgaven:  
[beskrivelse af ps kommandoen](https://man7.org/linux/man-pages/man1/ps.1.html)
  
## Instruktioner
I det første trin skal du se hvilken processer den nuværende bruger har kørende  
1. Eksekver kommandoen `ps`. _Resulatet skulle gerne ligne nedstående billede_  
![PS kommandoen](./Images/PSCommand.jpg)  
I venstre side af outputtet ses Process Id(PID). Hver process i Linux har et  unikt Id.
Her efter kommer  _TeleTypewriter_(TTY), som viser hvilken terminal eksekvert en kommando.
cpu tid(TIME), viser det samlet CPU tidsforbrug.
Til sidst viser Command(CMD) hvilken kommando der har eksekveret processen.  
2. Eksekver kommandoen `ps -aux`   
![PS AUX kommandoen](./Images/PSAUXCommand.jpg)  _Denne gang vises processerne for alle bruger_  
  
Der er et par nye kolonner i outputtet, de mest intressante er %CPU,%MEM og STAT.
%CPU og %MEM viser hver især det procentvise CPU og Hukommelses forbrug.
STAT viser processorens tilstand: S=Sleep but interuptible,R=Running & T=Terminated  
  
I de næste tre trin installer vi en service i form af Apache web server.
Typisk bruger man ordet service, når en applikation tilbyder en "service" til andre. I dette 
tilfælde en API via netværk (også i slang kaldet en "Web API"), som ande systemer karn benytte sig af (F.eks. en browser).
En service er blot en applikation som eksekveres på operativ systemet, og den har derfor også en proces.

3. Eksekver kommandoen `sudo apt install apache2 & sudo systemctl unmask apache2`
4. Udskriv alle processer til konsolen, og `grep` på ordet apache2.  
  
Bemærk tilstanden(STAT) er S. Altså processen køre ikke, men venter på en hændelse. I de næste trin
prøver vi at vække den.

5. Eksekver kommandoen `sudo apt install curl`. _Curl er et værktøj som kan lave http forespørgelser til F.eks. Apache2 web server_
6. Eksekver kommandoen `curl 127.0.0.1`. _Nu sendes der et HTTP request til Apache2 web serveren_
7. Gennemse outputtet, og bekræft at det er HTML fra Apache serveren.
  
Det betyder at selv om en process ikke køre(S), kan den ved behov vækkes af operativ systemet i tilfælde af en specifik hændelse.
  
I de sidste trin fokuseres der på at genstarte en proces, og at "Slå den ihjel".

8. Eksekver kommandoen `nano &`. _Nano er en text editor som vi med ampersand symbolet(&) kør i baggrunden_
9. Udskriv alle processer til konsolen, og grep på ordet 'nano'.   
_Det kommer formodentlig to resultater, den ene er grep kommandoen som lige er blevet eksekveret_
10. i kolonne 2 kan du se process id(PID), eksekver følgende kommando med process id'et `kill -1 <Skriv PID her>`
11.  Udskriv alle processer til konsolen, og grep på ordet 'nano'. outputtet skulle gerne ligne outputtet fra tidligere. Det fordi processen er genstartet, men ikke slukket, start tid og PID forbliver det samme.
12.  Eksekver nu følgende kommando `kill -9 <Skriv PID her>`
13.  Nu skulle processen være slukket, og ikke synlige hvis du udskriver processerne.




   
## Links
[Basic Linux commands](https://www.hostinger.com/tutorials/linux-commands)  
[Basic linux commands videos](https://www.youtube.com/watch?v=gd7BXuUQ91w)
  
