# Opgave 9 - Bruger kontoer i Linux.

## Information

I de følgende opgaver skal der arbejdes med oprettelse,ændring og fjernelse af bruger i Linux systemet.

I Linux er det kun brugere med superbruger rettigheder som kan fortag ændring ift. bruger kontoer på Linux
systemet. Men da emnet _Bruger rettigheder_ endnu ikke er introduceret er der først en kort gennemgang af superbruger
systemet i Linux. Formålet er at du forstå hvordan kommandoen `sudo` kan bruges.   
  
### Baggrunden for sudo kommandoen
Bruger systemet i Linux (og andre operativ systemer) har til formål at segmenterer enkelte brugersrettigheder,  
Således at hver enkelt bruger kun kan udføre arbejde på operativ systemet indenfor den enkelte brugers bemyndigelse ramme(Tænk: privilege of least).
Endevider giver bruger systemet en mulighed for at sikre sporbarhed ift. hvilken operationer den enkelte bruger har udført på operativ systemet(Accountability,repudiation,audit).
Derfor bør der for hver fysisk bruger der tilgår et Linux system, eksister en særskilt bruger konto.
_En bruger konto i Linux omtales ofte som login_
  
Linux bruger systemet har som minnimum altid 1 bruger kaldet _root_, og nogen gange benævnt _superbruger_ kontoen.
Root kontoen har adgang til alt i et Linux system. Det vil sige den kan læse,slette,oprette & redigerer
alle filer samt directories.
_Root kontoen kan dog deaktiveres_

En enkelt superbruger konto er dog ikke hensigtsmæssig. Med udgangspunkt i at hver fysisk bruger altid skal tilgå
et Linux system med en særskilt konto, er der en konflikt, såfremt mere end 1 bruger har behov for super bruger rettigheder.
Sporbarheden ift. den enkelte brugers handling i systemet vil ikke kunne opretholdes. Derudover er en superbruger konto det
man kalder et _Single point of failure_ i kontekst med det enkelte Linux system. Altså hvis en konto som har rettigheder til alt
bliver kompromittteret er hele systemet kompromitteret. Risikoen for at en konto bliver kompromitteret gennem F.eks. password læk
eller lignende stiger desto flere der har adgang til passwordet.

For at løse udfordringen med superbruger rettigheder til flere bruger i Linux,  bruger man `Sudo` (Super user do) som er en kommando der
kan bruges af alle som er medlem af en gruppe kaldet _sudoers_.

I Ubuntu er den bruger man indledningsvist opretter (Typisk under installationen) altid medlem af gruppen _sudoers_.

For at eksekver en kommando med superbruger rettigheder eksekveres kommandoen således `sudo <Kommando>`.

## Kommandoer til oprettelse, ændring og fjernelse af brugerer.

- [useradd - opret en ny bruger konto](https://linux.die.net/man/8/useradd)
- [userdel - Slet en bruger konto](https://linux.die.net/man/8/userdel)
- [usermod - ændring af en eksisterende bruger konto](https://linux.die.net/man/8/usermod)
- [passwd - ændring af brugers password (blandt andet)](https://man7.org/linux/man-pages/man1/passwd.1.html)
- [su - skift til anden bruger konto](https://man7.org/linux/man-pages/man1/su.1.html)
  
## Instruktioner
I de følgende opgaver skal der løses en række opgaver relateret til bruger.
Alle opgaver kan løses med en af de kommandoer som er referet overover.  
Som hjælp kan du også bruge de værktøjer som du lært i _Opgave 5 - Hjælp i Linux_

### Opret en ny bruger.
I denne opgave skal der oprettes en ny bruger ved navn `Mandalorian`.

### Tildel en bruger password
I denne opgave skal brugeren `Mandalorian`, havde tildelt et password.

### Ændring af eksisterende bruger konto opsætning.
I denne opgave skal `Mandalorian` bruger kontoen ændres således
den har en udløbs dato 23-06-2023.

### Ændring af brugerens hjemme directory.
I denne opgave skal bruger kontoen `Mandalorian` havde ændret sit hjemme
directory til `/home/mandoFiles/`. _husk at oprette directoriet først_.

### Filer tilknyttet en bruger.
I Linux er alle filer ejet af en bestemt bruger. Typisk når en bruger
opretter en file, får denne bruger også tildelt ejerskab over filen.

Med kommandoen `ls <filenavn> -al` kan du få vist en tilladelse for en specifik
file. Som vist på billede nedenunder:  
![Bruger rettigheder på en file](./Images/UserRights.jpg)  
Formatet er som vist nedenunder  
|Ejerens rettigheder|-|Gruppens rettigheder|-|Alle andres rettigheder|Antal links til filen| Ejer af filen| tilknyttet gruppe | _Forklaring på resten af kolonerne er bevidst undladt indtil videre_
  
Hvad enten man er på rød eller blå hold, er det nyttigt at kunne finde de filer som er tilknytet en
specifik konto. Dette kan man gøre med kommandoen `find` sammen med flaget `-user`.
_flaget user kan også bruges_

**I denne opgave skal følgende trin udføres:**  
1. opret tre filer med brugeren `Mandalorian`.  
2. find dem med `find` kommandoen, eksekver søgning fra `/`.  
_Bemærk de mange "Permission denied" og den lange søge tid_
  
### Slet en bruger.
I denne opgave skal brugeren `Mandalorian` slettes, udfør opgaven som følgende.

1. slet brugeren `Mandalorian` med userdel.
2. Find en af de filer som blevet oprettet af brugeren `Mandalorian`
3. Se rettighederne for denne file, og noter hvem der er file ejer.
4. Opret en bruger ved navn `Ivan`.
5. Gentag trin 3(Samme file)
6. Vurder om dette er en potetienelle sårbarhed, og om man bør overeje altid at slette/skifte ejerskab på filer når en bruger slettes  
_Et alternativ til at slette bruger kontoer, er at deaktiver dem_  
7. Overvej om princippet _Secure by default_ reelt er overholdt

### Filer tilknyttet bruger systemet - opbevaring af bruger kontoer
I linux er der som udgangspunkt to filer som er intressant ift. bruger systemet.
Den ene er filen passwd. I de fleste distributioner kan denne findes i stien `/etc/passwd`.
passwd indeholder alle informationer om bruger kontoer(undtaget passwords).

I denne opgave skal du udforske passwd filen.

1. Se rettighederne for passwd med kommandoen `ls <filenavn> -al`.
2. Overvej om rettighederne som udgangspunkt ser hensigtmæssige ud? og hvorfor der er de rettigheder som der er.
3. Udskriv filens indhold _Hvis du har glemt hvordan så kig i dit cheat sheet. øvelsen blev udført i opgave 7_
4. Samhold filens indhold med nedstående format (BEMÆRK: ikke alle kolonner er nødvendigvis tilstede)  
`Brugernavn:Password(legacy, ikke brugt længere derfor x istedet):Bruger id:Gruppe id:Bruger information:Hjemme directory:Default shell`

### Filer tilknyttet bruger systemet - passwords
Filen `shadow` opbevare passwords til alle bruger kontoer.
Alle passwords i `shadow` er hashes, som sikring mod udvekommende adgang til bruger kontoer.

I opgaven skal du udforske filen `shadow`.

1. udskriver rettighederne for filen `shadow`
2. Overvej rettighederne i samhold med `Privilege of least` princippet.
3. Udskriv filens indhold.
4. Samhold filens indhold med nedstående format.  
`:brugernavn:password(hashet):Sidste ændring af password:Minimum dage ind password skift:Maksimum dage ind password skift:Varslings tid for udløbet password:Antal dage med udløbet password inden dekativering af konto:konto udløbs dato`  
  
Nogen af jer er endnu ikke introduceret til password hashes, hvilket er okay. I skal blot noter
i jeres cheatsheet at i kan finde en forklaring på hashes i shadow filen i denne opgave.

Følgende viser et eksempel på et hash i shadow filen. 
`$y$j9T$GfMsEAQ8t9EkjiOiDzVRA0$cWqq3SrEZED3EvJYMFD/G1TYn8lgWOaSM8IvjCeD4j2:19417`  
formattet er som følger:  
`$hash algoritme id$salt$has$`  
Hash algoritmernes id kan man slå op. I eksemplet er `y` brugt. Det vil sige at hash algoritmen til at 
generer hashen er `yescrypt`.  

### Sårbarhed ved svage passwords.
Tiltrods for at yescrypt som udgangspunkt er en stærk algoritme. Så yder hashet kun begrænset
beskyttelse til svage kodeord. Dette skal der eksperimenteres med i denne opgave.
I de følgende trin skal der oprettes en bruger med et svagt kodeord. Herefter skal vi 
trække kodeord hashet ud fra shadow filen, og forsøg genskabe passwordet ud
fra hash værdien. 

**Alle kommandoer skal eksekveres mens du er i dit hjemme directory**  
1. installer værktøjer `john-the-ripper` med kommandoen `sudo apt install john`  
2. Her efter skal du downloade en _wordlist_ kaldet rockyou med følgende kommando `wget https://github.com/brannondorsey/naive-hashcat/releases/download/data/rockyou.txt`.  
3. Opret nu en bruger ved navn `misternaiv` og giv ham passwordet `password`  
4. Hent nu det hashet kodeord for misternaiv med følgende kommando `sudo cat /etc/shadow | grep misternaiv > passwordhash.txt`  
5. udskriv indholdet af filen `passwordhash.txt`, og bemærk hvilken krypterings algoritme der er brugt.  
6. Eksekver nu kommandoen `john -wordlist=rockyou.txt passwordhash.txt`.  
7. Kommandoen resulter i `No password loaded`. Dette er fordi john the ripper værktøjet ikke selv har kunne detekter hvilken algoritme det drejer sig om.  
8. Eksekver nu kommandoen:   `john --format=crypt -wordlist=rockyou.txt passwordhash.txt`.  
   _format fortæller john the ripper hvilken type algoritme det drejer sig om_  
9.  Resultat skulle gerne være at du nu kan se kodeordet, som vist på billede nedenunder.  
![password crack](./Images/passwordCracket.jpg)  
  
1.  gentag processen fra trin 1 af. Men med et stærkt password.(minnimum 16 karakterer, både store og små bogstaver, samt special tegn)  
2.  Reflekter over hvorfor komplekse kodeord er vigtige (Og andre gange en hæmmesko for sikkerheden).  
  



## Links
- [Et eksempel på hvordan man kan lave krav til kodeord kompleksiten i Linux](https://www.xmodulo.com/set-password-policy-linux.html)
- [Grundlæggende bruger styring i Linux](https://www.youtube.com/watch?v=19WOD84JFxA)
- [useradd - opret en ny bruger konto](https://linux.die.net/man/8/useradd)
- [userdel - Slet en bruger konto](https://linux.die.net/man/8/userdel)
- [usermod - ændring af en eksisterende bruger konto](https://linux.die.net/man/8/usermod)
- [passwd - ændring af brugers password (blandt andet)](https://man7.org/linux/man-pages/man1/passwd.1.html)
- [su - skift til anden bruger konto](https://man7.org/linux/man-pages/man1/su.1.html)
  
  
